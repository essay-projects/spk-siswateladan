<?php

    include_once 'connection.php';
    $id = $_POST['krit'];
    $bobotTambah = $_POST['bobot'];

    $sqlKrit = "SELECT * FROM kriteria where `id_kriteria` = '$id' LIMIT 1";
    $exeKrit = $conn->query($sqlKrit);
    $bobot = number_format(mysqli_fetch_assoc($exeKrit)['bobot_sensitifitas']+$bobotTambah,2);

    
    // echo $bobot;

    $sqlReset = "SELECT * FROM kriteria";
    $resultReset = $conn->query($sqlReset);

    while($kriteria = $resultReset->fetch_assoc()){
        $idKrit = $kriteria['id_kriteria'];
        if($idKrit == $id){
          $namaKrit = $kriteria['nama_kriteria'];
        }
        $conn->query("UPDATE `kriteria` set `bobot_sensitifitas` =  `bobot_kriteria` where `id_kriteria` = $idKrit ");
    }


    $query  = $conn->query("UPDATE `kriteria` set `bobot_sensitifitas` =  '$bobot' where `id_kriteria` = $id ");

    // if($query){
    //   header('location: analisis-sensitifitas.php');
    // }
    $totalS = 0;
    $totalS2 = 0;
    $sql = "SELECT `kode`,`namalengkap` FROM alternatif";
    $result = $conn->query($sql);
    while($alternatif = $result->fetch_assoc()) {
            $id = $alternatif['kode'];
            $sqlKrit = "SELECT r.`id_kriteria`,k.`tipe_kriteria`, k.`bobot_sensitifitas`, r.`nilai_rangking` FROM `alternatif` a JOIN `rangking` r ON a.`kode` = r.`kode` JOIN `kriteria` k ON r.`id_kriteria` = k.`id_kriteria` JOIN `nilai` n ON r.`id_kriteria` = n.`id_kriteria` WHERE r.`nilai_rangking` = n.`jum_nilai` AND a.`kode` = '$id'";
            $result2 = $conn->query($sqlKrit);
            $nilaiS = 1;
            $nilaiS2 = 1;
        while($kriteria = $result2->fetch_assoc()){

            $idKriteria = $kriteria['id_kriteria'];
            $sqlKrit = "SELECT `bobot_kriteria` as 'nilai', `bobot_sensitifitas` as 'nilai2' FROM kriteria where `id_kriteria` = '$idKriteria' LIMIT 1";
            $exeKrit = $conn->query($sqlKrit);
            $kriteriaPangkat = mysqli_fetch_assoc($exeKrit);
            $pangkat = $kriteriaPangkat['nilai'];		
            $pangkat2 = $kriteriaPangkat['nilai2'];
            if ($kriteria['tipe_kriteria'] == "cost"){
                $pangkat = "-".$kriteriaPangkat['nilai'];
                $pangkat2 = "-".$kriteriaPangkat['nilai2'];
            } 
            $nilaiSAlt = pow($kriteria['nilai_rangking'], $pangkat); 
            $nilaiSAlt2 = pow($kriteria['nilai_rangking'], $pangkat2); 
            // echo $nilaiSAlt;
            $nilaiS *= $nilaiSAlt;  
            $nilaiS2 *= $nilaiSAlt2;  
        }
        $totalS = $totalS + $nilaiS;                       
        $totalS2 = $totalS2 + $nilaiS2;                       

    }
    
                $sql = "SELECT `kode`,`namalengkap` FROM alternatif";
                $result = $conn->query($sql);
                while($alternatif = $result->fetch_assoc()) {

                    $nilaiS = 1;
                    $nilaiS2 = 1;
                    $hasil = 0;
                    $hasilSen= 0;
                    $id = $alternatif['kode'];
                    // echo $id;
                    $sqlKrit = "SELECT k.`bobot_kriteria`, k.`bobot_sensitifitas`, r.`id_kriteria`,k.`tipe_kriteria`, r.`nilai_rangking` FROM `alternatif` a JOIN `rangking` r ON a.`kode` = r.`kode` JOIN `kriteria` k ON r.`id_kriteria` = k.`id_kriteria` JOIN `nilai` n ON r.`id_kriteria` = n.`id_kriteria` WHERE r.`nilai_rangking` = n.`jum_nilai` AND a.`kode` = '$id'";
                    $result2 = $conn->query($sqlKrit);
                      while($kriteria = $result2->fetch_assoc()){
                          $idKriteria = $kriteria['id_kriteria'];
                          $sqlKrit = "SELECT `bobot_kriteria` as 'nilai', `bobot_sensitifitas` as 'nilai2' FROM kriteria where `id_kriteria` = '$idKriteria' LIMIT 1";
                          $exeKrit = $conn->query($sqlKrit);
                          $kriteriaPangkat = mysqli_fetch_assoc($exeKrit);
                          $pangkat = $kriteriaPangkat['nilai'];		
                          $pangkat2 = $kriteriaPangkat['nilai2'];		
                          if ($kriteria['tipe_kriteria'] == "cost"){
                              $idKriteria = $kriteria['id_kriteria'];
                              $sqlMin = "SELECT MIN(`nilai_rangking`) as 'nilai' FROM rangking where `id_kriteria` = '$idKriteria' LIMIT 1";
                              $exeMin = $conn->query($sqlMin);
                              $min = mysqli_fetch_assoc($exeMin);
                              $nor = ($min['nilai']/$kriteria['nilai_rangking'])*$kriteria['bobot_kriteria'];
                              $norSen = ($min['nilai']/$kriteria['nilai_rangking'])*$kriteria['bobot_sensitifitas'];
                              $pangkat = "-".$kriteriaPangkat['nilai'];
                              $pangkat2 = "-".$kriteriaPangkat['nilai2'];
                          } else {
                              $idKriteria = $kriteria['id_kriteria'];
                              $sqlMax = "SELECT MAX(`nilai_rangking`) as 'nilai' FROM rangking where `id_kriteria` = '$idKriteria' LIMIT 1";
                              $exeMax = $conn->query($sqlMax);
                              $max = mysqli_fetch_assoc($exeMax);
                              $nor = ($kriteria['nilai_rangking']/$max['nilai'])*$kriteria['bobot_kriteria'];
                              $norSen = ($kriteria['nilai_rangking']/$max['nilai'])*$kriteria['bobot_sensitifitas'];
                              // echo number_format($nor,5);
                          }
                          $hasil+=$nor;
                          $hasilSen+=$norSen;
                          $nilaiSAlt = pow($kriteria['nilai_rangking'], $pangkat); 
                          $nilaiSAlt2 = pow($kriteria['nilai_rangking'], $pangkat2); 

                          // echo $nilaiSAlt;
                          $nilaiS *= $nilaiSAlt;  
                          $nilaiS2 *= $nilaiSAlt2;  

                          // echo $kriteria['nilai_rangking']; 
                        } 
                  $nilaisaw = number_format($hasil,5);
                  $nilaisaw2 = number_format($hasilSen,5); 
                  $nilaiwp = number_format($nilaiS/$totalS,5);                         
                  $nilaiwp2 = number_format($nilaiS2/$totalS2,5);
                  $id = $alternatif['kode'];
                        
                  $sqlupdate = "UPDATE `hasil` SET `hasil_wp` = '$nilaiwp',`hasil_wp_sensitifitas` = '$nilaiwp2',`hasil_saw` = '$nilaisaw',`hasil_saw_sensitifitas` = '$nilaisaw2' WHERE `kode` = '$id'";
                  $i = $conn->query($sqlupdate);
                  if(!$i){
                    echo "gagal";
                  }
                  // echo $sqlupdate;

                }

                //update rangking wp normal
                $sql = "SELECT * FROM `hasil` ORDER BY `hasil`.`hasil_wp`  DESC";
                $result = $conn->query($sql);
                $i = 1;
                while($hasil = $result->fetch_assoc()) {
                  $id = $hasil['kode'];
                  $sqlupdate = "UPDATE `hasil` SET `rangking_wp` = '$i' WHERE `kode` = '$id'";
                  $conn->query($sqlupdate);
                  $i++;
                }

                 //update rangking wp sensitifitas
                 $sql = "SELECT * FROM `hasil` ORDER BY `hasil`.`hasil_wp_sensitifitas`  DESC";
                 $result = $conn->query($sql);
                 $i = 1;
                 while($hasil = $result->fetch_assoc()) {
                   $id = $hasil['kode'];
                   $sqlupdate = "UPDATE `hasil` SET `rangking_wp_sensitifitas` = '$i' WHERE `kode` = '$id'";
                   $conn->query($sqlupdate);
                   $i++;
                 }

                 //update rangking saw normal
                $sql = "SELECT * FROM `hasil` ORDER BY `hasil`.`hasil_saw`  DESC";
                $result = $conn->query($sql);
                $i = 1;
                while($hasil = $result->fetch_assoc()) {
                  $id = $hasil['kode'];
                  $sqlupdate = "UPDATE `hasil` SET `rangking_saw` = '$i' WHERE `kode` = '$id'";
                  $conn->query($sqlupdate);
                  $i++;
                }

                 //update rangking saw sensitifitas
                 $sql = "SELECT * FROM `hasil` ORDER BY `hasil`.`hasil_saw_sensitifitas`  DESC";
                 $result = $conn->query($sql);
                 $i = 1;
                 while($hasil = $result->fetch_assoc()) {
                   $id = $hasil['kode'];
                   $sqlupdate = "UPDATE `hasil` SET `rangking_saw_sensitifitas` = '$i' WHERE `kode` = '$id'";
                   $conn->query($sqlupdate);
                    $i++;
                 }
                $pesan = "Kriteria ".$namaKrit." ditambah ".$bobotTambah;
                
                $diffwp = 0;
                $diffsaw = 0;
                $sql = "SELECT * FROM `hasil`";
                 $result = $conn->query($sql);
                 $i = 1;
                 while($hasil = $result->fetch_assoc()) {
                   if($hasil['rangking_wp']!=$hasil['rangking_wp_sensitifitas']){
                     $diffwp++;
                   }
                   if($hasil['rangking_saw']!=$hasil['rangking_saw_sensitifitas']){
                    $diffsaw++;
                  }
                 }
                 $conn->query("INSERT INTO `hasil_history` (`id`, `kriteria`, `diff_wp`, `diff_saw`) VALUES (NULL, '$pesan', '$diffwp', '$diffsaw');");
                header('location: analisis-sensitifitas.php');

            
?>
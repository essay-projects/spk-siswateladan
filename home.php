<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Pengertian</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/half-slider.css" rel="stylesheet">

  </head>

  <body>

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg fixed-top" style="background-color: green; "> 
    <!-- ganti warna bg -->
      <div class="container">
        <a class="navbar-brand" href="index.php" style="color: #ffffff">HOME</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="nav navbar-nav">

            
            <li class="nav-item active">
              <a class="nav-link" href="home.php" style="color: #ffffff">Metode
                <span class="sr-only">(current)</span>
              </a>
            </li>
            <li class="nav-item active">
              <a class="nav-link" href="kriteria.php" style="color: #ffffff">Kriteria
                <span class="sr-only">(current)</span>
              </a>
            </li>

             <li class="nav-item active">
              <a class="nav-link" href="alternatif.php" style="color: #ffffff">Alternatif
                <span class="sr-only">(current)</span>
              </a>
            </li>

            <li class="nav-item">
              <a class="nav-link" href="sawwp.php" style="color: #ffffff">Perhitungan</a>
            </li>
          </ul>
        </div>
      </div>

    </nav>

    <div class="container-fluid p-0" style="background-color: lightgreen"><center>

    <br>
    <br>
    <section class="resume-section p-3 p-lg-5 d-flex align-items-center" id="interests">
      <div class="w-100">
        <img src="layout/pic/saw.png"  width="1200px"> 
        <br>
        <br>
        <h2 class="mb-5">METODE SIMPLE ADDITIVE WEIGHTING (SAW)</h2>
        <p>Metode SAW merupakan metode yang menggunakan skor alternatif untuk dihitung sebagai jumlah tertimbang nilai atribut dan membutuhkan proses normalisasi pada matriks keputusan (X) dalam suatu skala yang bisa dibandingkan dengan semua alternatif yang ada. </p>
      </div>
    </section>

    <section class="resume-section p-3 p-lg-5 d-flex align-items-center" id="interests">
      <div class="w-100">
        <img src="layout/pic/wp.png"  width="1200px"> 
        <br>
        <br>
        <h2 class="mb-5">WEIGHTED PRODUCT (WP)</h2>
        <p>Metode WP merupakan metode yang menggunakan teknik perkalian untuk menghubungkan rating setiap atribut. Metode ini menggunakan bobot setiap kriteria untuk bahan perhitungan.</p>
      </div>
    </section>

    <section class="resume-section p-3 p-lg-5 d-flex align-items-center" id="interests">
      <div class="w-100">
         <img src="layout/pic/as.png"  width="1000px"> 
        <br>
        <br>
        <h2 class="mb-5">ANALISIS SENSITIVITAS</h2>
        <p>Analisis sensitivitas merupakan analisis yang berkaitan dengan perubahan diskrit parameter untuk melihat berapa besar perubahan dapat ditolerir sebelum solusi optimum mulai kehilangan optimalisasinya.</p>
      </div>
    </section>

  </div>
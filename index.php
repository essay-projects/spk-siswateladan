<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Home</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/half-slider.css" rel="stylesheet">

  </head>

  <body>

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg fixed-top" style="background-color: green; "> 
    <!-- ganti warna bg -->
      <div class="container">
        <a class="navbar-brand" href="index.php" style="color: #ffffff">HOME</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="nav navbar-nav">
			<li class="nav-item active">
              <a class="nav-link" href="home.php" style="color: #ffffff">Metode
                <span class="sr-only">(current)</span>
              </a>
            </li>
            <li class="nav-item active">
              <a class="nav-link" href="kriteria.php" style="color: #ffffff">Kriteria
                <span class="sr-only">(current)</span>
              </a>
            </li>

             <li class="nav-item active">
              <a class="nav-link" href="alternatif.php" style="color: #ffffff">Alternatif
                <span class="sr-only">(current)</span>
              </a>
            </li>

            <li class="nav-item">
              <a class="nav-link" href="sawwp.php" style="color: #ffffff">Perhitungan</a>
            </li>
          </ul>
        </div>
      </div>

    </nav>

    <header>
      <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        
        <div class="carousel-inner" role="listbox">
          <!-- Slide One - Set the background image for this slide in the line below -->
          <div class="carousel-item active" style="background-image: url('layout/pic/mtsn5.JPG')">
            <div class="carousel-caption d-none d-md-block">
              <h5>ANALISIS SENSITIVITAS METODE SIMPLE ADDITIVE WEIGHTING (SAW) DAN WEIGHTED PRODUCT (WP)  </h5>
              <p> Pemilihan Siswa Teladan di MTs Negeri 5 Cirebon </p>
             
            </div>
          </div>

        </div>
        
      </div>
    </header>

    <!-- Page Content -->
    <section class="py-5">
      <div class="container" style="text-align: center;">
        
            <h2>VISI MTS NEGERI 5 CIREBON</h2>
            <p>"Unggul Dalam Prestasi, Teruji Dalam Mutu, Berbasis Pada Iman Dan Taqwa"</p>

            <h2>MISI MTS NEGERI 5 CIREBON</h2>
            <p></p>
            <p>Menumbuhkan Penghayatan Dan Pengalaman Terhadap Ajaran Islam, Sehingga Siswa Memiliki Kompetensi Dan Akhlaqul Karimah</p>
            <P>Menumbuhkan Semangat Keunggulan Kepada Warga Madrasah Kepada Imtaq</P>
            <p>Melaksanakan Pembelajaran Dan Bimbingan Secara Efektif Dan Efisien Secara Potensi Yang Dimiliki</p>
            <p>Mendorong Dan Membantu Setiap Siswa Untuk Mengenali Potensi Dirinya Sehingga Dapat Berkembang Secara Maksimal</p>
            <p>Menumbuhkan Dan Mendorong Dalam Penerapan Imtaq Dan Iptek Kepada Warga Madrasah</p>
            <p>Mendorong Lulusan Yang Berkualitas, Bermutu, Berprestasi, Beriman Dan Bertakwa Kepada Allah SWT</p>

           
          </div>
        </div>
      </div>
    </section>

    <!-- Footer -->
    <footer class="py-4 bg-dark">
      <div class="container">
        <p class="m-0 text-center text-white">Copyright &copy; Maghfira Izzani Rahmayani || 16650017</p>
      </div>
      <!-- /.container -->
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  </body>

</html>


 <?php
  include_once 'connection.php';

  include "includes/config.php";
  $config = new Config();
  $db = $config->getConnection();

  include_once 'includes/kriteria.inc.php';
  $kriteria = new Kriteria($db);
  ?>
<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Tambah</title>
    <script type="text/javascript" charset="utf8" src="js/jquery.min.js"></script>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link rel="stylesheet" type="text/css" href="css/jquery.dataTables.min.css" rel="stylesheet">

    <script type="text/javascript" charset="utf8" src="js/jquery.dataTables.min.js"></script>
  </head>

<body>
  <nav 
  style="background: green;" class="navbar navbar-inverse navbar-fixed-top bg-dark">
    <div class="container" style="font-color: #000">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
     
      <a class="navbar-brand" href="index.php" style="color: #ffffff">HOME</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="nav navbar-nav">
            <li class="nav-item active">
              <a class="nav-link" href="home.php" style="color: #ffffff">Metode
                <span class="sr-only">(current)</span>
              </a>
            </li>
            <li class="nav-item active">
              <a class="nav-link" href="kriteria.php" style="color: #ffffff">Kriteria
                <span class="sr-only">(current)</span>
              </a>
            </li>

             <li class="nav-item active">
              <a class="nav-link" href="alternatif.php" style="color: #ffffff">Alternatif
                <span class="sr-only">(current)</span>
              </a>
            </li>

            <li class="nav-item">
              <a class="nav-link" href="sawwp.php" style="color: #ffffff">Perhitungan</a>
            </li>
          </ul>
        </div>
      
        </ul>
      </li>
      </ul>
    </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
  </nav>

   <?php
  $mode="add";
  $nama_kriteria = "";
  $tipe_kriteria="";
  $bobot_kriteria="";
  if(!empty($_POST['submit'])) {
     $arr=[
        $_POST['nama_kriteria'],
        $_POST['tipe_kriteria'],
        $_POST['bobot_kriteria'],
        $_POST['bobot_kriteria']
     ];

     $hasil_kriteria = $kriteria->insertKriteria($arr);
     if($hasil_kriteria['is_success']) {

     }

     ?>
     <script>
         alert("Berhasil Di tambahkan");
         document.location="kriteria.php";
     </script>
     <?php
  }

  if(!empty($_GET['mode']) && $_GET['mode']=='edit' && !empty($_GET['id_kriteria'])) {
      $kriteria->id_kriteria = $_GET['id_kriteria'];   
      $kriteria->readOne();
 
      if($kriteria->id_kriteria!='') {
         $mode="edit";
         $nama_kriteria = $kriteria->nama_kriteria;
         $tipe_kriteria = $kriteria->tipe_kriteria;
         $bobot_kriteria=$kriteria->bobot_kriteria;
      }
  }

   if(!empty($_GET['mode']) && $_GET['mode']=='delete' && !empty($_GET['id_kriteria'])) {
      $kriteria->id_kriteria = $_GET['id_kriteria'];   
      $kriteria->readOne();

      if($kriteria->id_kriteria!='') {
         $mode="delete";
      }
   }

  if($mode=="edit") {
   if(!empty($_POST['update'])) {
      $err=[
        $_POST['nama_kriteria'],
        $_POST['tipe_kriteria'],
        $_POST['bobot_kriteria'],
        $_POST['bobot_kriteria']
      ];
 
      $kriteria->updateKriteria($err);
 
      ?>
      <script>
          alert("Berhasil Di edit");
          document.location="kriteria.php";
      </script>
      <?php
   }
  }

  if($mode=="delete") {
      $hasil_delete = $kriteria->deleteKriteria($_GET['id_kriteria']);
      if($hasil_delete) {
         ?>
         <script>
            alert("Berhasil Di hapus");
            document.location="kriteria.php";
         </script>
      <?php   
      }
   }
?>

            <!-- page content -->
  <body style="background-color: lightgreen;"> </body>
  <div class="container" style="padding-top: 60px"> <center>
      <h2>CRUD Kriteria</h2>
      <form class="form-horizontal form-label-left" action="" enctype="multipart/form-data" method="post"> <center>

         <table>
            <td>Nama Kriteria</td>
            <td>:</td>
            <td><input type="text" name="nama_kriteria" value="<?php echo $nama_kriteria; ?>" required></td>
         </tr>

         <tr>
            <td>Tipe Kriteria</td>
            <td>:</td>
            <td><input type="text" name="tipe_kriteria" value="<?php echo $tipe_kriteria; ?>" required></td>
         </tr>

         <tr>
            <td>Bobot Kriteria</td>
            <td>:</td>
            <td><input type="text" name="bobot_kriteria" value="<?php echo $bobot_kriteria; ?>" required></td>
         </tr>
         </table>
         <br/>
         <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3"> <br><br>
               <?php 
               if($mode=="edit") {
               ?>
               <button type="submit" class="btn btn-success" name="update" value="update">Update</button>
               <?php } else { ?>  
               <button type="submit" class="btn btn-success" name="submit" value="submit">Submit</button>
               <?php } ?> 
            <button type="button" onclick="window.history.go(-1)" class="btn btn-primary" >Kembali</button>
            </div>
         </div>

     </form>
   </div>
</div>
            </div>
            <!-- /page content -->

            <!-- footer content -->
          <?php
  include_once 'footer.php';
  ?>

      
         </body>
</html>

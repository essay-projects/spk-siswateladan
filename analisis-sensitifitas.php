<?php

    include_once 'connection.php';
    
    //total nilai WP
    $totalS = 0;
    $totalS2 = 0;
    $sql = "SELECT `kode`,`namalengkap` FROM alternatif";
    $result = $conn->query($sql);
    while($alternatif = $result->fetch_assoc()) {
            $id = $alternatif['kode'];
            $sqlKrit = "SELECT r.`id_kriteria`,k.`tipe_kriteria`, k.`bobot_sensitifitas`, r.`nilai_rangking` FROM `alternatif` a JOIN `rangking` r ON a.`kode` = r.`kode` JOIN `kriteria` k ON r.`id_kriteria` = k.`id_kriteria` JOIN `nilai` n ON r.`id_kriteria` = n.`id_kriteria` WHERE r.`nilai_rangking` = n.`jum_nilai` AND a.`kode` = '$id'";
            $result2 = $conn->query($sqlKrit);
            $nilaiS = 1;
            $nilaiS2 = 1;
        while($kriteria = $result2->fetch_assoc()){

            $idKriteria = $kriteria['id_kriteria'];
            $sqlKrit = "SELECT `bobot_kriteria` as 'nilai', `bobot_sensitifitas` as 'nilai2' FROM kriteria where `id_kriteria` = '$idKriteria' LIMIT 1";
            $exeKrit = $conn->query($sqlKrit);
            $kriteriaPangkat = mysqli_fetch_assoc($exeKrit);
            $pangkat = $kriteriaPangkat['nilai'];		
            $pangkat2 = $kriteriaPangkat['nilai2'];
            if ($kriteria['tipe_kriteria'] == "cost"){
                $pangkat = "-".$kriteriaPangkat['nilai'];
                $pangkat2 = "-".$kriteriaPangkat['nilai2'];
            } 
            $nilaiSAlt = pow($kriteria['nilai_rangking'], $pangkat); 
            $nilaiSAlt2 = pow($kriteria['nilai_rangking'], $pangkat2); 
            // echo $nilaiSAlt;
            $nilaiS *= $nilaiSAlt;  
            $nilaiS2 *= $nilaiSAlt2;  
        }
        $totalS = $totalS + $nilaiS;                       
        $totalS2 = $totalS2 + $nilaiS2;                       

    }


?>
<!DOCTYPE html>
<html>
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Analisis Sensitivitas</title>
		<script type="text/javascript" charset="utf8" src="js/jquery.min.js"></script>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
		<link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">
    <!-- Custom styles for this template -->
		<link rel="stylesheet" type="text/css" href="css/jquery.dataTables.min.css" rel="stylesheet">

		<script type="text/javascript" charset="utf8" src="js/jquery.dataTables.min.js"></script>
  </head>

<body>

<nav 
	style="background: green;" class="navbar navbar-inverse navbar-fixed-top bg-dark">
	  <div class="container" style="font-color: #000">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="container">
        <a class="navbar-brand" href="index.php" style="color: #ffffff">HOME</a>
       
         <span class="navbar-toggler-icon"></span>
        	<div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="nav navbar-nav">
            <li class="nav-item active">
              <a class="nav-link" href="home.php" style="color: #ffffff">Metode
                <span class="sr-only">(current)</span>
              </a>
            </li>
            <li class="nav-item active">
              <a class="nav-link" href="kriteria.php" style="color: #ffffff">Kriteria
                <span class="sr-only">(current)</span>
              </a>
            </li>

             <li class="nav-item active">
              <a class="nav-link" href="alternatif.php" style="color: #ffffff">Alternatif
                <span class="sr-only">(current)</span>
              </a>
            </li>
            <li class="nav-item active">
              <a class="nav-link" href="sawwp.php" style="color: #ffffff">Hasil Perhitungan
                <span class="sr-only">(current)</span>
              </a>
            </li>
		  </ul>
		</div><!-- /.navbar-collapse -->
	  </div><!-- /.container-fluid -->
	</nav>
<body style="background-color: lightgreen;"> </body> 
	<div class="container" style="padding-top: 60px">
			<div class="tabel-saw">
            <div class="row">
                <form method="post" action="tambah-bobot.php" name="kriteria" class="my_text col-md-6" id="ins"> 
                    <div class="form-group col-md-12">
                        <h4 style="text-align: center; color:black">PILIH KRITERIA YANG DIINGINKAN</h4>
                        <div class="col-xs-12" style="padding-bottom: 5px;">
                            <select class="form-control positionTypes" id="krit" name="krit" required>
                                <option value="" disabled selected> Pilih kriteria </option>
                                <?php
                                $sql = "SELECT * FROM kriteria";
                                $result = $conn->query($sql);
                                while ($row = $result->fetch_assoc()){
                                    extract($row);
                                    echo "<option value='{$id_kriteria}'>{$nama_kriteria}</option>";
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group col-md-12">
                        <h4 style="text-align: center; color:black">PILIH KENAIKAN BOBOT YANG DIINGINKAN</h4>
                        <div class="col-xs-12" style="padding-bottom: 5px;">
                            <input name="bobot" class="form-control positionTypes" type="number" step="0.01" value="0.1" max="2">
                        </div>
                    </div>
                    <div class="form-group" style="padding-right:15px">
                        <input type="submit" name="pilih" class="btn btn-success btn-xs" style="float: right; background-color: darkred;" value="Pilih">
                    </div>
                </form>
            </div>
            <div class="tabel-saw" style="text-align: center;">
			<h2>KRITERIA YANG DI INPUT</h2>
			<table class="table table-striped table-bordered" style="border: 10px solid: #ddd !important;">
			  <thead style="background: darkgreen; color:white">
			    <tr>
			      	<th scope="col" style="text-align: center;">Kode</th>
			      	<th scope="col" style="text-align: center;">Nama Kriteria</th>
			    	<th scope="col" style="text-align: center;">Jenis</th>
					<th scope="col" style="text-align: center;">Bobot Kriteria</th>
					<th scope="col" style="text-align: center;">Bobot Uji Sensitifitas</th>
			    </tr>
			  </thead>
			  <tbody >
					<?php
							$sql = "SELECT * FROM kriteria";
							$result = $conn->query($sql);
							while($row = $result->fetch_assoc()) {
					?>
					<tr style="background: #E0E0E0; color:black">
							<td style="text-align: center;">
									<?= $row['id_kriteria'] ?>
							</td>
							<td>
									<?= $row['nama_kriteria'] ?>
							</td>
							<td style="text-align: center;">
									<?= $row['tipe_kriteria'] ?>
							</td>
							<td style="text-align: center;">
									<?= $row['bobot_kriteria'] ?>
							</td>
							<td style="text-align: center;">
									<?= $row['bobot_sensitifitas'] ?>
							</td>
					</tr>
				<?php
						}
            ?>
			  </tbody>
			</table>
		</div>
            
                <h2>HASIL PERBANDINGAN</h2>
                <p>Data Nilai Siswa</p>
			<table class="table table-striped table-bordered" style="border: 10px solid: #ddd !important;" id="table1">
			  <thead style="background: darkgreen; color:white">
			    <tr>
				  <th scope="col" style="text-align: center;">Kode</th>
			      <th scope="col" style="text-align: center;">Nama </th>
			      <th scope="col" style="text-align: center;">Nilai Normal SAW</th>
			      <th scope="col" style="text-align: center;">Nilai Uji Sensitifitas SAW</th>
			      <th scope="col" style="text-align: center;">Nilai Normal WP</th>
			      <th scope="col" style="text-align: center;">Nilai Uji Sensitifitas WP</th>
			    </tr>
			  </thead>
			  <tbody>
            <?php
                $sql = "SELECT a.`kode`,a.`namalengkap`, h.`hasil_wp`, h.`hasil_wp_sensitifitas`, h.`hasil_saw`, h.`hasil_saw_sensitifitas` FROM `alternatif` a JOIN `hasil` h ON a.`kode` = h.`kode`";
                $result = $conn->query($sql);
                while($alternatif = $result->fetch_assoc()) {
                    $nilaiS = 1;
                    $nilaiS2 = 1;
                    $hasil = 0;
                    $hasilSen= 0;
                    // $id = $alternatif['kode'];
                    // $sqlKrit = "SELECT a.`kode`,a.`namalengkap`, h.`hasil_wp`, h.`hasil_wp_sensitifitas`, h.`hasil_saw`, h.`hasil_saw_sensitifitas` FROM `alternatif` a JOIN `hasil` h ON a.`kode` = h.`kode`";
                    // $result2 = $conn->query($sqlKrit);
                    // $hasil = mysqli_fetch_assoc($result2);
            ?>
                <tr <tr style="background: #E0E0E0; color:black">
                    <td style="text-align: center;">
                        <?= $alternatif['kode'] ?>
                    </td>
                    <td>
                        <?= $alternatif['namalengkap'] ?>
                    </td>
                    <td style="text-align: center;"><?= number_format($alternatif['hasil_saw'],5); ?></td>
                    <td style="text-align: center;"><?= number_format($alternatif['hasil_saw_sensitifitas'],5); ?></td>
                    <td style="text-align: center;"><?= number_format($alternatif['hasil_wp'],5); ?></td>
                    <td style="text-align: center;"><?= number_format($alternatif['hasil_wp_sensitifitas'],5); ?></td>
                    
                </tr>
            <?php
                }
            ?>
        </tbody>
			</table>
      <h2>HASIL UJI SENSITIVITAS</h2>
      <a class="btn btn-danger btn-xs" style="background: darkred" href="del-history.php?id=all">Reset</a><br>
			<table class="table table-striped table-bordered" style="border: 10px solid: #ddd !important" id="table2">
			  <thead style="background: darkgreen; color: white">
			    <tr>
			      <th scope="col" style="text-align: center;">NO</th>
			      <th scope="col" style="text-align: center;">Perubahan Bobot Kriteria</th>
			      <th scope="col" style="text-align: center;">Perubahan Rangking SAW</th>
          		  <th scope="col" style="text-align: center;">Perubahan Rangking WP</th>
			    </tr>
			  </thead>
			  <tbody >
					<?php
              $i=1;
							$sql = "SELECT * FROM hasil_history";
							$result = $conn->query($sql);
							while($row = $result->fetch_assoc()) {
					?>
					<tr style="background: #E0E0E0; color:black">
							<td style="text-align: center;">
									<?= $i++; ?>
							</td>
							<td>
									<?= $row['kriteria'] ?>
							</td>
							<td style="text-align: center;">
									<?= $row['diff_saw'] ?>
							</td>
							<td style="text-align: center;">
									<?= $row['diff_wp'] ?>
							</td>
					</tr>
				<?php
						}
            ?>
			  </tbody>
			</table>
      <?php 
        $sqlPerubahan = "SELECT COUNT(*) as `perulangan`, SUM(`diff_wp`) as `wp` , SUM(`diff_saw`) as `saw` FROM `hasil_history`";
        $exePerubahan = $conn->query($sqlPerubahan);
        $totalPerubahan = mysqli_fetch_assoc($exePerubahan);
        
        $sqlJumlahKrit = "SELECT COUNT(*) as `jumlahKrit` FROM `kriteria`";
        $exeJumlahKrit = $conn->query($sqlJumlahKrit);
        $jumlahKrit = mysqli_fetch_assoc($exeJumlahKrit)['jumlahKrit'];
        if($totalPerubahan['perulangan'] != 0){
          $diffsaw = $totalPerubahan['saw'] / ($totalPerubahan['perulangan'] * $jumlahKrit);
          $diffwp = $totalPerubahan['wp'] / ($totalPerubahan['perulangan'] * $jumlahKrit)
          ?>
          <p style="text-align: center; color:black">PERUBAHAN SIMPLE ADDITIVE WEIGHTING = <?= $diffsaw ?>%</p>
          <p style="text-align: center; color:black">PERUBAHAN WEIGHTED PRODUCT = <?= $diffwp ?>%</p>

        <?php
          $sqlRekomendasi = "SELECT * FROM `alternatif` a JOIN `hasil` h ON a.`kode` = h.`kode` ORDER BY h.`hasil_wp` DESC LIMIT 10";
          if($diffsaw>$diffwp){
            $sqlRekomendasi = "SELECT * FROM `alternatif` a JOIN `hasil` h ON a.`kode` = h.`kode` ORDER BY h.`hasil_saw` DESC LIMIT 10";
          } else if ($diffwp<$diffsaw) {
          	$sqlRekomendasi = "SELECT * FROM `alternatif` a JOIN `hasil` h ON a.`kode` = h.`kode` ORDER BY h.`hasil_wp` DESC LIMIT 10";
          }
          $exeRekomendasi = $conn->query($sqlRekomendasi);
          if (mysqli_num_rows($exeRekomendasi) ) { ?>


          <h2>HASIL REKOMENDASI SISWA SETELAH DILAKUKAN ANALISIS SENSITIVITAS</h2>
           <p>Semakin besar hasil nilai sensitivitas maka metode itu semakin sensitif</p>
			<table class="table table-striped table-bordered" style="border: 10px solid: #ddd !important;" id="table3">
			  <thead style="background: darkgreen; color:white">
			    <tr>
			      <th scope="col" style="text-align: center;">No</th>
			      <th scope="col" style="text-align: center;">Kode</th>
			      <th scope="col" style="text-align: center;">Rekomendasi Siswa</th>
			      <th scope="col" style="text-align: center;">Alamat</th>
			      <th scope="col" style="text-align: center;">Angkatan</th>
			      <th scope="col" style="text-align: center;">Kelas</th>

			    </tr>
			  </thead>
			  <tbody >
          <?php
			$i=0;
			while($row = $exeRekomendasi->fetch_assoc()) { ?>

					<tr style="background: #E0E0E0; color:black">
							<td style="text-align: center;">
									<?= ++$i; ?>
							</td>
							<td style="text-align: center;">
									<?= $row['kode'] ?>
							</td>
							<td>
									<?= $row['namalengkap'] ?>
							</td>
							<td>
									<?= $row['alamat'] ?>
							</td>
							<td style="text-align: center;">
									<?= $row['angkatan'] ?>
							</td>
							<td style="text-align: center;">
									<?= $row['kelas'] ?>
							</td>
					</tr>
	
      <?php } ?>
      			<?php
			}
						}
            ?>
			  </tbody>
			</table>
		</div>
	</div>
	<script type="text/javascript">
		$(document).ready( function () {
				$('#table1').DataTable();
		} );
	</script>
	<script type="text/javascript">
		$(document).ready( function () {
				$('#table2').DataTable();
		} );
	</script>
	<script type="text/javascript">
		$(document).ready( function () {
				$('#table3').DataTable();
		} );
	</script>
  <!-- Footer -->
    <footer>
      <div class="container">
        <div class="row">
             <p class="m-0 text-center text-white">Copyright &copy; Maghfira Izzani Rahmayani || 16650017</p>
          </div>
        </div>
      </div>

    </footer>
	
</body>

</html>
<?php

	include_once 'connection.php';

?>
<!DOCTYPE html>
<html>
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Alternatif</title>
		<script type="text/javascript" charset="utf8" src="js/jquery.min.js"></script>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
		<link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">
    <!-- Custom styles for this template -->
		<link rel="stylesheet" type="text/css" href="css/jquery.dataTables.min.css" rel="stylesheet">

		<script type="text/javascript" charset="utf8" src="js/jquery.dataTables.min.js"></script>
  </head>

<body>
	<nav 
	style="background: green;" class="navbar navbar-inverse navbar-fixed-top bg-dark">
	  <div class="container" style="font-color: #000">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
		 
		  <a class="navbar-brand" href="index.php" style="color: #ffffff">HOME</a>
		</div>

		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="nav navbar-nav">
          	<li class="nav-item active">
              <a class="nav-link" href="home.php" style="color: #ffffff">Metode
                <span class="sr-only">(current)</span>
              </a>
            </li>
            <li class="nav-item active">
              <a class="nav-link" href="kriteria.php" style="color: #ffffff">Kriteria
                <span class="sr-only">(current)</span>
              </a>
            </li>

             <li class="nav-item active">
              <a class="nav-link" href="alternatif.php" style="color: #ffffff">Alternatif
                <span class="sr-only">(current)</span>
              </a>
            </li>

            <li class="nav-item">
              <a class="nav-link" href="sawwp.php" style="color: #ffffff">Perhitungan</a>
            </li>
          </ul>
        </div>
			
			  </ul>
			</li>
		  </ul>
		</div><!-- /.navbar-collapse -->
	  </div><!-- /.container-fluid -->
	</nav>

	<body style="background-color: lightgreen;"> </body>
	<div class="container" style="padding-top: 60px">
		<div class="tabel-saw" style="text-align: center; color:black">
			<h2>DATA SISWA KELAS 8 MTs Negeri 5 Cirebon</h2>
	        <div style="width:100%;"><a href="tambah.php" style="float:right;">+ Tambah</a></div>
			<table class="table table-striped table-bordered" style="border: 10px solid: #ddd !important;" id="table1">
			  <thead style="background: green; color: white">
			    <tr>
			      <th><center>Kode Alternatif</th>
			      <th><center>Nama Lengkap</th>
			      <th><center>Alamat </th>
				  <th><center>Angkatan </th>
				  <th><center>Kelas</th>
				  <th><center>Aksi</th></tr>
			  </thead>
			  <tbody >
					<?php
							$sql = "SELECT * FROM alternatif";
							$result = $conn->query($sql);
							while($row = $result->fetch_assoc()) {
					?>
					<tr style="background: #E0E0E0">
							<td>
									<?= $row['kode'] ?>
							</td>
							<td>
									<?= $row['namalengkap'] ?>
							</td>
							<td>
									<?= $row['alamat'] ?>
							</td>
							<td>
									<?= $row['angkatan'] ?>
							</td>
							<td>
									<?= $row['kelas'] ?>
							</td>
							<td>
									<a href="tambah.php?mode=edit&kode=<?php echo $row['kode']; ?>">Edit</a> | <a style="cursor:pointer;" onclick="confirmBeforeDelete('<?php echo $row['kode']; ?>')">Delete</a>
							</td>
					</tr>
				<?php
						}
            ?>
			  </tbody>
			</table>
		</div>
	</div>
<script>
  function confirmBeforeDelete(kode) {
    var r = confirm("Apakah anda yakin");
    if (r == true) {
      document.location="tambah.php?mode=delete&kode="+kode;
    }
  }
</script>
	<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
	<script type="text/javascript">
		$(document).ready( function () {
				$('#table1').DataTable();
		} );

		
		</script>
		    
		     
    <!-- Footer -->
    <footer>
      <div class="container">
        <div class="row">
             <p class="m-0 text-center text-white">Copyright &copy; Maghfira Izzani Rahmayani || 16650017</p>
          </div>
        </div>
      </div>

 	</footer>

</body>

</html>
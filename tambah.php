
 <?php
  include_once 'connection.php';

  include "includes/config.php";
  $config = new Config();
  $db = $config->getConnection();

  include_once 'includes/alternatif.inc.php';
  $alter = new Alternatif($db);

  include_once 'includes/rangking.inc.php';
  $rangking = new bobot($db);

  include_once 'includes/kriteria.inc.php';
  $kriteria = new Kriteria($db);
  $kriteria_read_all = $kriteria->readAll();
  ?>
<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Tambah</title>
    <script type="text/javascript" charset="utf8" src="js/jquery.min.js"></script>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link rel="stylesheet" type="text/css" href="css/jquery.dataTables.min.css" rel="stylesheet">

    <script type="text/javascript" charset="utf8" src="js/jquery.dataTables.min.js"></script>
  </head>

<body>
  <nav 
  style="background: green;" class="navbar navbar-inverse navbar-fixed-top bg-dark">
    <div class="container" style="font-color: #000">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
     
      <a class="navbar-brand" href="index.php" style="color: #ffffff">HOME</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="nav navbar-nav">
            <li class="nav-item active">
              <a class="nav-link" href="home.php" style="color: #ffffff">Metode
                <span class="sr-only">(current)</span>
              </a>
            </li>
            <li class="nav-item active">
              <a class="nav-link" href="kriteria.php" style="color: #ffffff">Kriteria
                <span class="sr-only">(current)</span>
              </a>
            </li>

             <li class="nav-item active">
              <a class="nav-link" href="alternatif.php" style="color: #ffffff">Alternatif
                <span class="sr-only">(current)</span>
              </a>
            </li>

            <li class="nav-item">
              <a class="nav-link" href="sawwp.php" style="color: #ffffff">Perhitungan</a>
            </li>
          </ul>
        </div>
      
        </ul>
      </li>
      </ul>
    </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
  </nav>

   <?php
  $mode="add";
  $namalengkap = "";
  $alamat="";
  $angkatan="";
  $kelas ="";
  if(!empty($_POST['submit'])) {
     $arr=[
        $_POST['namalengkap'],
        $_POST['alamat'],
        $_POST['angkatan'],
        $_POST['kelas']
     ];

     $hasil_alternatif = $alter->insertAlternatif($arr);
     if($hasil_alternatif['is_success']) {
         while($data_kriteria = $kriteria_read_all->fetch(PDO::FETCH_ASSOC)){
            $arr=[
                  $hasil_alternatif['kode'],
                  $data_kriteria['id_kriteria'],
                  $_POST['kriteria_'.$data_kriteria['id_kriteria']]
            ];
            $rangking->insertBobot($arr);
         }

         $rangking->insertAnalisaSensistifitas($hasil_alternatif['kode']);
     }

     ?>
     <script>
         alert("Berhasil Di tambahkan");
         document.location="alternatif.php";
     </script>
     <?php
  }

  if(!empty($_GET['mode']) && $_GET['mode']=='edit' && !empty($_GET['kode'])) {
      $alter->kode = $_GET['kode'];   
      $alter->readOne();
 
      if($alter->kode!='') {
         $mode="edit";
         $namalengkap = $alter->namalengkap;
         $alamat = $alter->alamat;
         $angkatan=$alter->angkatan;
         $kelas=$alter->kelas;
      }
  }

   if(!empty($_GET['mode']) && $_GET['mode']=='delete' && !empty($_GET['kode'])) {
      $alter->kode = $_GET['kode'];   
      $alter->readOne();

      if($alter->kode!='') {
         $mode="delete";
      }
   }

  if($mode=="edit") {
   if(!empty($_POST['update'])) {
      $arr=[
        $_POST['namalengkap'],
        $_POST['alamat'],
        $_POST['angkatan'],
        $_POST['kelas'],
        $_GET['kode']
      ];
 
      $hasil_alternatif = $alter->updateAlternatif($arr);
      if($hasil_alternatif) {
          while($data_kriteria = $kriteria_read_all->fetch(PDO::FETCH_ASSOC)){
             $arr=[
                   $_POST['kriteria_'.$data_kriteria['id_kriteria']],
                   $_GET['kode'],
                   $data_kriteria['id_kriteria']
             ];
             $rangking->updateBobot($arr);
          }
      }
 
      ?>
      <script>
          alert("Berhasil Di edit");
          document.location="alternatif.php";
      </script>
      <?php
   }
  }

  if($mode=="delete") {
      $hasil_delete = $alter->deleteAlternatif($_GET['kode']);
      if($hasil_delete) {
         ?>
         <script>
            alert("Berhasil Di hapus");
            document.location="alternatif.php";
         </script>
      <?php   
      }
   }
?>

            <!-- page content -->
  <body style="background-color: lightgreen;"> </body>
  <div class="container" style="padding-top: 60px"> <center>
      <h2>Tambah Data Siswa</h2>
      <form class="form-horizontal form-label-left" action="" enctype="multipart/form-data" method="post"> <center>

         <table>
            <td>Nama Siswa</td>
            <td>:</td>
            <td><input type="text" name="namalengkap" value="<?php echo $namalengkap; ?>" required></td>
         </tr>

         <tr>
            <td>Alamat</td>
            <td>:</td>
            <td><input type="text" name="alamat" value="<?php echo $alamat; ?>" required></td>
         </tr>

         <tr>
            <td>Angkatan</td>
            <td>:</td>
            <td><input type="text" name="angkatan" value="<?php echo $angkatan; ?>" required></td>
         </tr>


         <tr>
            <td>Kelas</td>
            <td>:</td>
            <td><input type="text" name="kelas" value="<?php echo $kelas; ?>" required></td>
         </tr>
         </table>
         <br/>
         <h3> Masukkan Bobot Per Kriteria </h3>
         <table>
         <?php
         while($data_kriteria = $kriteria_read_all->fetch(PDO::FETCH_ASSOC)){
            $val=1;
            if ($mode=="edit") {
               $rangking->kode=$_GET['kode'];
               $rangking->id_kriteria=$data_kriteria['id_kriteria'];
               $val = $rangking->readBobot();
            }
         ?>
         <tr>
            <td><?php echo $data_kriteria['nama_kriteria']; ?></td>
            <td>:</td>
            <td><input type="number" min="1" max="5" value="<?php echo $val; ?>" name="kriteria_<?php echo $data_kriteria['id_kriteria']; ?>"></td>
         </tr>
         <?php
         }
         ?>
         </table>

         <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3"> <br><br>
               <?php 
               if($mode=="edit") {
               ?>
               <button type="submit" class="btn btn-success" name="update" value="update">Update</button>
               <?php } else { ?>  
               <button type="submit" class="btn btn-success" name="submit" value="submit">Submit</button>
               <?php } ?> 
            <button type="button" onclick="window.history.go(-1)" class="btn btn-primary" >Kembali</button>
            </div>
         </div>

     </form>
   </div>
</div>
            </div>
            <!-- /page content -->

            <!-- footer content -->
          <?php
  include_once 'footer.php';
  ?>

      
         </body>
</html>

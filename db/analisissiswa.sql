-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 11 Bulan Mei 2020 pada 17.10
-- Versi server: 10.4.11-MariaDB
-- Versi PHP: 7.4.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `analisissiswa`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `alternatif`
--

CREATE TABLE `alternatif` (
  `kode` int(5) NOT NULL,
  `namalengkap` varchar(100) NOT NULL,
  `alamat` varchar(225) NOT NULL,
  `angkatan` int(5) NOT NULL,
  `kelas` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `alternatif`
--

INSERT INTO `alternatif` (`kode`, `namalengkap`, `alamat`, `angkatan`, `kelas`) VALUES
(1, 'Siti Bathiah Balqis', 'Blok Pesantren RT/RW 006/003 Ds. Balerante Kec. Palimanan Kab. Cirebon', 2018, 'VIII A'),
(2, 'Intan Dewi Pertiwi', 'Kp. Palasari Kel. Kutalanggeng Kec. Tegalwaru Kab. Karawang', 2018, 'VIII A'),
(3, 'Nur Hoiriyah', 'Blok Pekulen Barat RT/RW 010/005 Ds. Balerante Kec. Palimanan Kab. Cirebon', 2018, 'VIII A'),
(4, 'Indah Nursakinah', 'Blok Karanganyar RT/RW 001/001 Ds. Cikeusal Kec. Gempol Kab. Cirebon', 2018, 'VIII A'),
(5, 'Ghina Puspita', 'Tengguli RT/RW 003/004 Ds. Tengguli Kec. Tanjung Kab. Brebes', 2018, 'VIII A'),
(6, 'Dhea Safitri', 'Blok Karanganyar RT/RW 001/003 Ds. Cikeusal Kec. Gempol Kab. Cirebon', 2018, 'VIII A'),
(7, 'Salsabila', 'Ds. Panongan RT/RW 002/002 Kec. Palimanan Kab. Cirebon', 2018, 'VIII A'),
(8, 'Pebryliyanti', 'Blok Telar Gaga RT/RW 001/001 Ds. Cikeusal Kec. Gempol Kab. Cirebon', 2018, 'VIII A'),
(9, 'Eva Tri Sulistiawati', 'Ds. Cikeusal RT/RW 006/002 Kec. Gempol Kab. Cirebon', 2018, 'VIII A'),
(10, 'Ika Mudrikah', 'Blok Telar Gandik RT/RW 04/02 Ds. Panongan Kec. Palimanan Kab. Cirebon', 2018, 'VIII A'),
(11, 'Lia Maulidina', 'Blok Pesantren RT/RW 005/003 Ds. Balerante Kec. Palimanan Kab. Cirebon', 2018, 'VIII B'),
(12, 'Adnan Dinata Hidaryan', 'Blok Kedung Dadap RT/RW 007/002 Ds. Beberan Kec. Palimanan Kab. Cirebon', 2018, 'VIII B'),
(13, 'Syifa Rahma Khairunnisa', 'Blok Masjid RT/RW 002/002 Ds. Panongan Kec. Palimanan Kab. Cirebon', 2018, 'VIII B'),
(14, 'Rizkulloh Eka Fitri Novia', 'Bojongsana RT/RW 012/004 Ds. Bojongsana Kec. Suradadi Kab. Tegal', 2018, 'VIII B'),
(15, 'Laela Wahyuni', 'Blok Kandang RT/RW 043/001 Ds. Palimanan Barat Kec. Gempol Kab. Cirebon', 2018, 'VIII B'),
(16, 'Imut Mutiyah Ningsih', 'Karang Kidul RT/RW 003/005 Ds. Winong Kec. Gempol Kab. Cirebon', 2018, 'VIII B'),
(17, 'Rahma Wati', 'Blok Pipisan RT/RW 003/001 Ds. Kedokan Agung Kec. Kedokan Bunder Kab. Indramayu', 2018, 'VIII B'),
(18, 'Ahmad Ramadan', 'Blok Kajengan Lor Ds. Danawinangun Kec. Klangenan Kab. Cirebon', 2018, 'VIII B'),
(19, 'Linda Amalia', 'Blok Pesantren RT/RW 006/003 Ds. Balerante Kec. Palimanan Kab. Cirebon', 2018, 'VIII B'),
(20, 'Rahma Nuraeni', 'Blok Ki Bangun RT/RW 003/002 Ds. Panongan Kec. Palimanan Kab. Cirebon', 2018, 'VIII B'),
(21, 'Selfiana Nurul Asifa', 'Blok Kedung Dadap RT/RW 012/002 Ds. Beberan Kec. Palimanan Kab. Cirebon', 2018, 'VIII C'),
(22, 'Fatimah Tuzzahro', 'Blok Tolok RT/RW 028/007 Ds. Palimanan Barat Kec. Gempol Kab. Cirebon', 2018, 'VIII C'),
(23, 'Dinda Nurrahmah', 'Blok Karang Baru RT/RW 007/003 Ds. Cikeusal Kec. Gempol Kab. Cirebon', 2018, 'VIII C'),
(24, 'Laela Safitri', 'Blok Pipisan RT/RW 004/001 Ds. Kedokan Agung Kec. Kedokan Bunder Kab. Indramayu', 2018, 'VIII C'),
(25, 'Siti Aisyah', 'Ds. Warujaya RT/RW 002/002 Kec. Depok Kab Cirebon', 2018, 'VIII C'),
(26, 'Ghaniyyah Hijriyanti', 'Blok Benggol RT/RW 001/005 Ds. Kepuh Kec. Palimanan Kab. Cirebon', 2018, 'VIII C'),
(27, 'Diva Ramadhani', 'Blok Kedung Bulus RT/RW 038/010 Ds. Palimanan Barat Kec. Gempol Kab. Cirebon', 2018, 'VIII C'),
(28, 'Angelika Fadillah', 'Blok Curug RT/RW 046/012 Ds. Palimanan Barat Kec. Gempol Kab. Cirebon', 2018, 'VIII C'),
(29, 'Lutfiatul Qolbiyah', 'Jl. Suropati Blok I RT/RW 001/002 Ds. Tegalgubug Kec. Arjawinangun Kab. Cirebon', 2018, 'VIII C'),
(30, 'Dewi Nurkholifah', 'Ds. Jatipura RT/RW 002/001 Kec. Susukan Kab. Cirebon', 2018, 'VIII C'),
(31, 'Koriah', 'Blok Gunung Kidul RT/RW 002/003 Kec. Dukupuntang Kab. Cirebon', 2018, 'VIII D'),
(32, 'Salwa Ayuning Tiyas', 'Blok Pesantren RT/RW 006/003 Ds. Balerante Kec. Palimanan Kab. Cirebon', 2018, 'VIII D'),
(33, 'Lu\'lu\' Qurrotul Aeni', 'Blok 6 RT/RW 026/009 Ds. Panguragan Kec. Panguragan Kab. Cirebon', 2018, 'VIII D'),
(34, 'Allyssia Berlin Kurniadi', 'Blok Jatimulya RT/RW 006/001 Ds. Jatimulya Kec. Suradadi Kab. Tegal', 2018, 'VIII D'),
(35, 'Nada Nazdiifah', 'Dusun Pon 2 RT/RW 004/00 Ds. Mertapada Wetan Kec.Astanajapura Kab. Cirebon', 2018, 'VIII D'),
(36, 'Shinta Adinda Putri', 'Blok Pasek RT/RW 036/009 Ds. Palimanan Barat Kec. Gempol Kab. Cirebon', 2018, 'VIII D'),
(37, 'Ulin Ni\'mah', 'Blok Kedung Dadap RT/RW 010/003 Ds. Beberan Kec. Palimanan Kab. Cirebon', 2018, 'VIII D'),
(38, 'Tita Nur Amaliyah', 'Blok BTN Abri C RT/RW 004/004 Ds Klangenan Kec. Klangenan Kab. Cirebon', 2018, 'VIII D'),
(39, 'Dwi Ayu Putri Andini', 'Ds. Cikeusal RT/RW 006/002 Kec. Gempol Kab. Cirebon', 2018, 'VIII D'),
(40, 'Siti Khodijah', 'Blok Pesantren RT/RW 006/003 Ds. Balerante Kec. Palimanan Kab. Cirebon', 2018, 'VIII D'),
(41, 'Indria Noor Shanty', 'Blok Pancuran Daris RT/RW 003/002 Ds. Balerante Kec. Palimanan Kab, Cirebon', 2018, 'VIII E'),
(42, 'Nurhalimah', 'Blok Pekulen Barat RT/RW 010/005 Ds. Balerate Kec. Palimanan Kab. Cirebon', 2018, 'VIII E'),
(43, 'Fina Durrun Nadhifah', 'Blok Setu Jero RT/RW 002/001 Kec. Weru Kab. Cirebon', 2018, 'VIII E'),
(44, 'Lubnatul Inayah', 'Blok Karang Moncol RT/RW 005/003 Ds. Pengabuan Kec. Lelea Kab. Indramayu', 2018, 'VIII E'),
(45, 'Sulastri', 'Blok Tengguli RT/RW 003/004 Ds. Tengguli Kec. Tanjung Kab. Brebes', 2018, 'VIII E'),
(46, 'Nurul Istiqomah', 'Blok Pasek RT/RW 035/009 Ds. Palimanan Barat Kec. Gempol Kab. Cirebon', 2018, 'VIII E'),
(47, 'Riska', 'Blok Panggang Lele RT/RW 004/001 Ds. Panongan Kec. Palimanan Kab. Cirebon', 2018, 'VIII E'),
(48, 'Nur Khasanah', 'Blok Pasek RT/RW 034/009 Ds. Palimanan Barat Kec. Gempol Kab. Cirebon', 2018, 'VIII E'),
(49, 'Nazwa Aulia', 'Blok Umburu RT/RW 005/001 Ds. Gintung Lor Kec. Susukan Kab. Cirebon', 2018, 'VIII E'),
(50, 'Abdullah', 'Blok Pondok Bata RT/RW 002/005 Ds. Palimanan Barat Kec. Gempol Kab. Cirebon', 2018, 'VIII E'),
(51, 'Habibatul Haibah', 'Blok Ki Bangun RT/RW 004/003 Ds. Panongan Kec. Palimanan Kab. Cirebon', 2018, 'VIII F'),
(52, 'Rosyidatur Rahmah', 'Blok Telar Gandik RT/RW 004/002 Ds. Panongan Kec. Palimanan Kab. Cirebon', 2018, 'VIII F'),
(53, 'Hizratol Azleah', 'Blok 2 Kamitaram RT/RW 001/003 Ds. Tegalgubug Kec. Arjawinangun Kab. Cirebon', 2018, 'VIII F'),
(54, 'Meysabrin Nazwa Andiny', 'Ds. Cikeusal RT/RW 004/001 Kec. Gempol Kab. Cirebon', 2018, 'VIII F'),
(55, 'Anggita Yuliarachman', 'Blok Telar Gandik RT/RW 004/002 Ds. Panongan Kec. Palimanan Kab. Cirebon', 2018, 'VIII F'),
(56, 'Yayang Nuraeni', 'Blok Gunung Santri RT/RW 001/003 Ds. Kedongdong Kidul Kec. Dukupuntang Kab. Cirebon', 2018, 'VIII F'),
(57, 'Anggun Rozzaqu', 'Blok Beberan RT/RW 004/001 Ds. Beberan Kec. Palimanan Kab. Cirebon', 2018, 'VIII F'),
(58, 'Andika Anugerah Setiawan', 'Blok Benda RT/RW 002/004 Ds. Warukawung Kec. Depok Kab. Cirebon', 2018, 'VIII F'),
(59, 'Dahliya', 'Blok Minggu RT/RW 003/003 Ds. Kedungkanca Kec. Ligung Kab. Majalengka', 2018, 'VIII F'),
(60, 'Puspa Rani Nirvana', 'Blok Pekulen Barat RT/RW 011/006 Ds. Balerante Kec. Palimanan Kab. Cirebon', 2018, 'VIII F'),
(61, 'Vinia Vinanda Dewi', 'BTN Korpri IV RT 002 Ds. Cempaka Kec. Plumbon Kab. Cirebon', 2018, 'VIII G'),
(62, 'Nazwatun Nabillah', 'Blok Masjid RT/RW 002/006 Ds. Sindangmekar Kec. Dukupuntang Kab. Cirebon', 2018, 'VIII G'),
(63, 'Nazwa Dil Aprilia', 'Blok Makmur RT/RW 002004 Ds. Panongan Kec. Palimanan Kab. Cirebon', 2018, 'VIII G'),
(64, 'Maulidah Hasanah', 'Ds. Cikeusal RT/RW 002/001 Kec. Gempol Kab. Cirebon', 2018, 'VIII G'),
(65, 'Ika Fitri Nurdiyanti', 'Kp. Palasari RT/RW 002/001 Ds. Kucalanggeng Kec. Tegalwaru Kab. Karawamg', 2018, 'VIII G'),
(66, 'Nurul Fauziah', 'Blok Pasek RT/RW 034/009 Ds. Palimanan Barat Kec. Gempol Kab. Cirebon', 2018, 'VIII G'),
(67, 'Gita Fatmah', 'Blok V Pilang RT/RW 003/005 Ds. Girinata Kec. Dukupuntang Kab. Cirebon', 2018, 'VIII G'),
(68, 'Fazwa Khoerul Fajar', 'Blok 01 Kamitraan Kidul RT/RW 002/003 Ds. Tegalgubug Kec. Arjawinangun Kab. Cirebon', 2018, 'VIII G'),
(69, 'Barokatul Jannah', 'Blok Widara RT/RW 008/002 Ds. Tegal Mulya Kec. Krangkeng Kab. Indramayu', 2018, 'VIII G'),
(70, 'Farkhatul Lailiyah', 'Blok Kedung Bulus RT/RW 038/010 Ds. Palimanan Barat Kec. Gempol Kab. Cirebon', 2018, 'VIII G'),
(71, 'Sofa Maulida', 'Blok Sigelap RT/RW 023/006 Ds. Palimanan Barat Kec. Gempol Kab. Cirebon', 2018, 'VIII A'),
(72, 'Desi Walna', 'Blok Seriyem RT/RW 012/003 Ds. Cilukrak Kec. Palimanan Kab. Cirebon', 2018, 'VIII A'),
(73, 'Nur Ikhwan', 'Blok Kedung Bulus RT/RW 038/010 Ds. Palimanan Barat Kec. Gempol Kab. Cirebon', 2018, 'VIII A'),
(74, 'Aan Anisa', 'Blok I RT/RW 005/001 Ds. Gintung Kec. Susukan Kab. Cirebon', 2018, 'VIII A'),
(75, 'Fatimah Azzhara', 'Blok Pasek Kulon RT/RW 042/011 Ds. Palimanan Barat Kec. Gempol Kab. Cirebon', 2018, 'VIII A'),
(76, 'Keisya Alyana', 'Blok Karang Dalem RT/RW 015/003 Ds. Jemaras Lor Kec. Klangenan Kab. Cirebon', 2018, 'VIII B'),
(77, 'Nurul Saputri', 'Blok Sikerta RT/RW 004/003 Ds. Panonga Kec. Palimanan Kab. Cirebon', 2018, 'VIII B'),
(78, 'Achmad Rizki', 'Blok Tolok RT/RW 028/007 Ds. Palimanan Barat Kec. Gempol Kab. Cirebon', 2018, 'VIII B'),
(79, 'Siti Nabilah Febriyanti', 'Blok Tolok RT/RW 026/007 Ds. Palimanan Barat Kec. Gempol Kab. Cirebon', 2018, 'VIII B'),
(80, 'Astrid Ainayyah Zaidin', 'Blok Pesantren RT/RW 006/003 Ds. Balerante Kec. Palimanan Kab. Cirebon', 2018, 'VIII B'),
(81, 'Riyana', 'Blok Tolok RT/RW 025/007 Ds. Palimanan Barat Kec. Gempol Kab. Cirebon', 2018, 'VIII C'),
(82, 'Giska', 'Blok Pekulen Barat RT/RW 011/006 Ds. Balerante Kec. Palimanan Kab. Cirebon', 2018, 'VIII C'),
(83, 'Nurdiyanti Pratiwi', 'Blok Karang Anyar RT/RW 004/002 Ds. Cikeusal Kec. Gempol Kab. Cirebon', 2018, 'VIII C'),
(84, 'Lusi Damayanti', 'Ds. Cikeusal RT/RW 006/001 Kec. Gempol Kab. Cirebon', 2018, 'VIII C'),
(85, 'Asria', 'Blok Panggang Lele RT/RW 004/001 Ds. Panongan Kec. Palimanan Kab. Cirebon', 2018, 'VIII C'),
(86, 'Tofik Bagus Ramadhan', 'Blok Pasek RT/RW 035/009 Ds. Palimanan Barat Kec. Gempol Kab. Cirebon', 2018, 'VIII D'),
(87, 'Tahlimatus Sa\'diyah', 'Blok Jatimulya RT/RW 004/001 Ds. Jatimulya Kec. Suradadi Kab. Tegal', 2018, 'VIII D'),
(88, 'Nur Adam', 'Blok Tolok RT/RW 027/007 Ds. Palimanan Barat Kec. Gempol Kab. Cirebon', 2018, 'VIII D'),
(89, 'Fatimah Azzahro', 'Blok Gumbira RT/RW 002/003 Ds. Semplo Kec. Palimanan Kab. Cirebon', 2018, 'VIII D'),
(90, 'Akmad Muzaqi', 'Blok Tegalampes v 016/005 Ds. Pringgacala Kec. Karangampel Kab. Indramayu', 2018, 'VIII D'),
(91, 'Rifqi Saefullah Almahdi', 'Kp. Klangenan RT/RW 001/002 Ds. Klangenan Kec. Klangenan Ksb. Cirebon', 2018, 'VIII E'),
(92, 'Muhamad Nurhasan', 'Blok Karang Kaum RT/RW 003/002 Ds. Panongan Kec. Palimanan Kab. Cirebon', 2018, 'VIII E'),
(93, 'Afif Izzatullah', 'Blok Tolok RT/RW 028/007 Ds. Palimanan Barat Kec. Gempol Kab. Cirebon', 2018, 'VIII E'),
(94, 'Mohamad Arsyad Diva', 'Blok 1 Pojok Teyong Kidul RT/RW 003/001 Ds. Tegalgubug Kec. Arjawinangun Kab. Cirebon', 2018, 'VIII E'),
(95, 'Aditya Rama', 'Blok Makmur RT/RW 004/004 Ds. Panongan Kec. Palimanan Kab. Cirebon', 2018, 'VIII E'),
(96, 'Tia Fadillah', 'Blok Pesantren RT/RW 006/003 Ds. Balerante Kec. Palimanan Kab. Cirebon', 2018, 'VIII F'),
(97, 'Naflatul Marwah', 'Ds. Balerante Kec. Palimanan Kab. Cirebon', 2018, 'VIII F'),
(98, 'Aan Anisa Putri', 'Blok Pipisan RT/RW 004/001 Ds. Kedokan Agung Kec. Kedokan Bunder Kab. Indramayu', 2018, 'VIII F'),
(99, 'Tri Dwiyansa', 'Blok Telar Gandik RT/RW 003/002 Ds. Panongan Kec. Palimanan Kab. Cirebon', 2018, 'VIII F'),
(100, 'Auralia Putri Hanifa', 'Blok I RT/RW 002/001 Ds. Bobos Kec. Dukupuntang Kab. Cirebon', 2018, 'VIII F'),
(101, 'Nur Syafiqul\'amin', 'Blok Pasek RT/RW 036/009 Ds. Palimanan Barat Kec. Gempol Kab. Cirebon', 2018, 'VIII G'),
(102, 'Aprillia Dewi Lestari', 'Blok Kemadu Kulon RT/RW 003/003 Ds. Kepuh Kec. Palimanan Kab. Cirebon', 2018, 'VIII G'),
(103, 'Ma\'rifatul Zakiyah', 'Blok Kalen Senen RT/RW RT/RW 006/002 Ds. Singajaya Kec. Indramayu Kab. Indramayu', 2018, 'VIII G'),
(104, 'Tri Naisah Wati', 'Kampung Mede RT/RW 006/002 Kel. Bekasi Jaya Kec. Bekasi Timur Kota Bekasi', 2018, 'VIII G'),
(105, 'Elis Indriani', 'Blok Kedung Bulus RT/RW 038/010 Ds. Palimanan Barat Kec. Gempol Kab. Cirebon', 2018, 'VIII G');

-- --------------------------------------------------------

--
-- Struktur dari tabel `hasil`
--

CREATE TABLE `hasil` (
  `id` int(11) NOT NULL,
  `kode` int(11) NOT NULL,
  `hasil_wp` double DEFAULT NULL,
  `hasil_wp_sensitifitas` double DEFAULT NULL,
  `hasil_saw` double DEFAULT NULL,
  `hasil_saw_sensitifitas` double DEFAULT NULL,
  `rangking_wp` int(11) DEFAULT NULL,
  `rangking_wp_sensitifitas` int(11) DEFAULT NULL,
  `rangking_saw` int(11) DEFAULT NULL,
  `rangking_saw_sensitifitas` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `hasil`
--

INSERT INTO `hasil` (`id`, `kode`, `hasil_wp`, `hasil_wp_sensitifitas`, `hasil_saw`, `hasil_saw_sensitifitas`, `rangking_wp`, `rangking_wp_sensitifitas`, `rangking_saw`, `rangking_saw_sensitifitas`) VALUES
(1, 1, 0.011752286535661, 0.01269, 0.880015, 1.88112, 3, 2, 2, 2),
(2, 2, 0.0097101184794246, 0.01049, 0.785135, 1.78624, 40, 34, 26, 24),
(3, 3, 0.01144534383411, 0.01236, 0.856295, 1.8574, 4, 4, 4, 4),
(4, 4, 0.0097101184794246, 0.01049, 0.785135, 1.78624, 35, 33, 27, 25),
(5, 5, 0.011176304781805, 0.01207, 0.834975, 1.83608, 7, 7, 7, 7),
(6, 6, 0.0094076525209712, 0.01016, 0.757635, 1.75874, 54, 42, 46, 49),
(7, 7, 0.0097048124183774, 0.01048, 0.78466, 1.78576, 42, 37, 29, 29),
(8, 8, 0.0097048124183774, 0.01048, 0.78466, 1.78576, 41, 35, 30, 28),
(9, 9, 0.011170197528265, 0.01206, 0.8345, 1.8356, 8, 8, 8, 8),
(10, 10, 0.0094766870275035, 0.01023, 0.76334, 1.76444, 45, 40, 38, 36),
(11, 11, 0.0097101184794246, 0.01049, 0.785135, 1.78624, 38, 31, 25, 27),
(12, 12, 0.0094818683620544, 0.01024, 0.763815, 1.76492, 44, 39, 37, 35),
(13, 13, 0.0094076525209712, 0.01016, 0.757635, 1.75874, 55, 48, 47, 43),
(14, 14, 0.0090193443161578, 0.00779, 0.719855, 1.52074, 72, 86, 74, 86),
(15, 15, 0.0097101184794246, 0.01049, 0.785135, 1.78624, 37, 32, 28, 26),
(16, 16, 0.0094076525209712, 0.01016, 0.757635, 1.75874, 48, 51, 48, 41),
(17, 17, 0.0090193443161578, 0.00779, 0.719855, 1.52074, 73, 85, 73, 85),
(18, 18, 0.0091865122952736, 0.00992, 0.736315, 1.73742, 59, 57, 59, 64),
(19, 19, 0.0094076525209712, 0.01016, 0.757635, 1.75874, 47, 49, 49, 40),
(20, 20, 0.0094076525209712, 0.01016, 0.757635, 1.75874, 56, 44, 50, 46),
(21, 21, 0.011055381306927, 0.01194, 0.8321, 1.8332, 16, 16, 9, 10),
(22, 22, 0.010016832381189, 0.01082, 0.81216, 1.81326, 33, 29, 17, 17),
(23, 23, 0.0094766870275035, 0.01023, 0.76334, 1.76444, 43, 41, 39, 37),
(24, 24, 0.0097813725182088, 0.01056, 0.79084, 1.79194, 34, 30, 24, 23),
(25, 25, 0.011055381306927, 0.01194, 0.8321, 1.8332, 15, 15, 10, 9),
(26, 26, 0.011745864538861, 0.01268, 0.87954, 1.88064, 2, 3, 3, 3),
(27, 27, 0.010716867193174, 0.01157, 0.805075, 1.80618, 21, 21, 22, 21),
(28, 28, 0.010828167759073, 0.01169, 0.807475, 1.80858, 17, 17, 19, 19),
(29, 29, 0.0088073318259777, 0.00761, 0.698535, 1.49942, 82, 90, 83, 90),
(30, 30, 0.0079776208767293, 0.00517, 0.633255, 1.23392, 98, 98, 99, 98),
(31, 31, 0.010716867193174, 0.01157, 0.805075, 1.80618, 19, 20, 21, 22),
(32, 32, 0.0091865122952736, 0.00992, 0.736315, 1.73742, 69, 56, 69, 65),
(33, 33, 0.0088073318259777, 0.00761, 0.698535, 1.49942, 78, 92, 79, 92),
(34, 34, 0.0083414861267667, 0.0054, 0.660755, 1.26142, 96, 96, 96, 96),
(35, 35, 0.0091865122952736, 0.00992, 0.736315, 1.73742, 68, 62, 65, 59),
(36, 36, 0.011118558953343, 0.01201, 0.831195, 1.8323, 11, 12, 13, 14),
(37, 37, 0.0091865122952736, 0.00992, 0.736315, 1.73742, 67, 60, 64, 57),
(38, 38, 0.010659631749383, 0.00921, 0.793415, 1.5943, 22, 73, 23, 81),
(39, 39, 0.0088073318259777, 0.00761, 0.698535, 1.49942, 79, 88, 80, 88),
(40, 40, 0.0067782132300148, 0.00146, 0.585195, 0.78542, 104, 104, 104, 104),
(41, 41, 0.011386207932341, 0.0123, 0.852515, 1.85362, 5, 6, 5, 5),
(42, 42, 0.0094076525209712, 0.01016, 0.757635, 1.75874, 52, 46, 51, 45),
(43, 43, 0.0079776208767293, 0.00517, 0.633255, 1.23392, 99, 99, 100, 99),
(44, 44, 0.0088073318259777, 0.00761, 0.698535, 1.49942, 80, 91, 81, 91),
(45, 45, 0.008997279862576, 0.00972, 0.730135, 1.73124, 74, 68, 72, 68),
(46, 46, 0.0091865122952736, 0.00992, 0.736315, 1.73742, 62, 64, 63, 61),
(47, 47, 0.0091865122952736, 0.00992, 0.736315, 1.73742, 63, 55, 56, 62),
(48, 48, 0.0097061887750067, 0.01048, 0.734935, 1.73604, 36, 36, 71, 67),
(49, 49, 0.0080895611549703, 0.00524, 0.639435, 1.2401, 97, 97, 98, 97),
(50, 50, 0.0086114396911343, 0.0093, 0.70311, 1.70421, 84, 72, 78, 72),
(51, 51, 0.011118558953343, 0.01201, 0.831195, 1.8323, 13, 13, 12, 11),
(52, 52, 0.0085204425558597, 0.0092, 0.687495, 1.6886, 86, 74, 86, 73),
(53, 53, 0.011075694678758, 0.01196, 0.829275, 1.83038, 14, 14, 16, 16),
(54, 54, 0.012820317451983, 0.01384, 0.896655, 1.89776, 1, 1, 1, 1),
(55, 55, 0.011118558953343, 0.01201, 0.831195, 1.8323, 9, 9, 15, 13),
(56, 56, 0.010782762356889, 0.01164, 0.809875, 1.81098, 18, 18, 18, 18),
(57, 57, 0.010148895298681, 0.01096, 0.762435, 1.76354, 30, 27, 41, 38),
(58, 58, 0.0085204425558597, 0.0092, 0.687495, 1.6886, 87, 80, 87, 79),
(59, 59, 0.0089090663083448, 0.00962, 0.714995, 1.7161, 77, 71, 77, 71),
(60, 60, 0.010148895298681, 0.01096, 0.762435, 1.76354, 29, 28, 40, 39),
(61, 61, 0.0085204425558597, 0.0092, 0.687495, 1.6886, 88, 75, 88, 74),
(62, 62, 0.0097061887750067, 0.01048, 0.734935, 1.73604, 39, 38, 70, 66),
(63, 63, 0.0085204425558597, 0.0092, 0.687495, 1.6886, 89, 76, 89, 75),
(64, 64, 0.010312405820721, 0.01114, 0.782375, 1.78348, 27, 26, 35, 34),
(65, 65, 0.0085204425558597, 0.0092, 0.687495, 1.6886, 90, 77, 90, 76),
(66, 66, 0.0085204425558597, 0.0092, 0.687495, 1.6886, 85, 78, 85, 77),
(67, 67, 0.0089090663083448, 0.00962, 0.714995, 1.7161, 76, 70, 76, 70),
(68, 68, 0.0085204425558597, 0.0092, 0.687495, 1.6886, 91, 79, 91, 78),
(69, 69, 0.0077366851628973, 0.00501, 0.611935, 1.2126, 101, 100, 101, 100),
(70, 70, 0.010312405820721, 0.01114, 0.782375, 1.78348, 26, 25, 34, 33),
(71, 71, 0.011118558953343, 0.01201, 0.831195, 1.8323, 10, 10, 11, 15),
(72, 72, 0.010464952018311, 0.0113, 0.783755, 1.78486, 24, 22, 31, 31),
(73, 73, 0.0094076525209712, 0.01016, 0.757635, 1.75874, 53, 52, 42, 48),
(74, 74, 0.010033002951035, 0.00867, 0.745975, 1.54686, 31, 84, 53, 84),
(75, 75, 0.0091865122952736, 0.00992, 0.736315, 1.73742, 60, 54, 58, 52),
(76, 76, 0.0094076525209712, 0.01016, 0.757635, 1.75874, 51, 47, 43, 44),
(77, 77, 0.0094076525209712, 0.01016, 0.757635, 1.75874, 49, 45, 44, 42),
(78, 78, 0.0083803891558331, 0.00905, 0.670065, 1.67117, 92, 81, 92, 80),
(79, 79, 0.0089641434765948, 0.00968, 0.717935, 1.71904, 75, 69, 75, 69),
(80, 80, 0.0094076525209712, 0.01016, 0.757635, 1.75874, 46, 43, 52, 50),
(81, 81, 0.010716867193174, 0.01157, 0.805075, 1.80618, 20, 19, 20, 20),
(82, 82, 0.010464952018311, 0.0113, 0.783755, 1.78486, 23, 24, 32, 30),
(83, 83, 0.0091865122952736, 0.00992, 0.736315, 1.73742, 61, 66, 67, 63),
(84, 84, 0.0091865122952736, 0.00992, 0.736315, 1.73742, 70, 63, 66, 60),
(85, 85, 0.011118558953343, 0.01201, 0.831195, 1.8323, 12, 11, 14, 12),
(86, 86, 0.0073894098388079, 0.00319, 0.595475, 0.99592, 102, 102, 102, 102),
(87, 87, 0.0079124394643232, 0.00341, 0.644295, 1.04474, 100, 101, 97, 101),
(88, 88, 0.0083414861267667, 0.0054, 0.660755, 1.26142, 95, 93, 95, 93),
(89, 89, 0.0094076525209712, 0.01016, 0.757635, 1.75874, 50, 50, 45, 47),
(90, 90, 0.0083414861267667, 0.0054, 0.660755, 1.26142, 93, 94, 93, 94),
(91, 91, 0.0088073318259777, 0.00761, 0.698535, 1.49942, 83, 87, 84, 87),
(92, 92, 0.0067782132300148, 0.00146, 0.585195, 0.78542, 105, 105, 105, 105),
(93, 93, 0.0083414861267667, 0.0054, 0.660755, 1.26142, 94, 95, 94, 95),
(94, 94, 0.0067782132300148, 0.00146, 0.585195, 0.78542, 103, 103, 103, 103),
(95, 95, 0.0091865122952736, 0.00992, 0.736315, 1.73742, 66, 61, 60, 58),
(96, 96, 0.0091865122952736, 0.00992, 0.736315, 1.73742, 65, 58, 57, 54),
(97, 97, 0.011386207932341, 0.0123, 0.852515, 1.85362, 6, 5, 6, 6),
(98, 98, 0.0091865122952736, 0.00992, 0.736315, 1.73742, 71, 65, 68, 55),
(99, 99, 0.0091865122952736, 0.00992, 0.736315, 1.73742, 64, 67, 55, 56),
(100, 100, 0.010033002951035, 0.00867, 0.745975, 1.54686, 32, 83, 54, 83),
(101, 101, 0.010274520130319, 0.00887, 0.767295, 1.56818, 28, 82, 36, 82),
(102, 102, 0.010464952018311, 0.0113, 0.783755, 1.78486, 25, 23, 33, 32),
(103, 103, 0.0088073318259777, 0.00761, 0.698535, 1.49942, 81, 89, 82, 89),
(104, 104, 0.0091865122952736, 0.00992, 0.736315, 1.73742, 57, 59, 62, 53),
(105, 105, 0.0091865122952736, 0.00992, 0.736315, 1.73742, 58, 53, 61, 51);

-- --------------------------------------------------------

--
-- Struktur dari tabel `hasil_history`
--

CREATE TABLE `hasil_history` (
  `id` int(11) NOT NULL,
  `kriteria` varchar(100) DEFAULT NULL,
  `diff_wp` int(11) DEFAULT NULL,
  `diff_saw` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `hasil_history`
--

INSERT INTO `hasil_history` (`id`, `kriteria`, `diff_wp`, `diff_saw`) VALUES
(1, 'Kriteria Nilai Skill ditambah 0.5', 93, 74),
(2, 'Kriteria Nilai Skill ditambah 0.5', 96, 74),
(3, 'Kriteria Pelanggaran ditambah 0.5', 68, 27),
(4, 'Kriteria Pelanggaran ditambah 0.5', 34, 27),
(5, 'Kriteria Kedisiplinan ditambah 0.5', 100, 101),
(6, 'Kriteria Kedisiplinan ditambah 0.5', 102, 103),
(7, 'Kriteria Ekstrakurikuler ditambah 0.5', 35, 67),
(8, 'Kriteria Ekstrakurikuler ditambah 0.5', 35, 67),
(9, 'Kriteria BTQ ditambah 0.5', 104, 103),
(10, 'Kriteria BTQ ditambah 0.5', 105, 103),
(11, 'Kriteria Pengetahuan Siswa ditambah 0.5', 80, 71),
(12, 'Kriteria Pengetahuan Siswa ditambah 0.5', 79, 71),
(13, 'Kriteria Prestasi ditambah 0.4', 45, 14),
(14, 'Kriteria Prestasi ditambah 0.5', 17, 14),
(15, 'Kriteria Absensi ditambah 0.5', 78, 85),
(16, 'Kriteria Absensi ditambah 0.5', 82, 85);

-- --------------------------------------------------------

--
-- Struktur dari tabel `kriteria`
--

CREATE TABLE `kriteria` (
  `id_kriteria` int(2) NOT NULL,
  `nama_kriteria` varchar(25) NOT NULL,
  `tipe_kriteria` varchar(25) NOT NULL,
  `bobot_kriteria` double DEFAULT NULL,
  `bobot_sensitifitas` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kriteria`
--

INSERT INTO `kriteria` (`id_kriteria`, `nama_kriteria`, `tipe_kriteria`, `bobot_kriteria`, `bobot_sensitifitas`) VALUES
(1, 'Nilai Skill', 'benefit', 0.1081, 0.1081),
(2, 'Pelanggaran', 'cost', 0.1325, 0.1325),
(3, 'Kedisiplinan', 'benefit', 0.1066, 0.1066),
(4, 'Ekstrakurikuler', 'benefit', 0.1186, 0.1186),
(5, 'BTQ', 'benefit', 0.11, 0.11),
(6, 'Pengetahuan Siswa', 'benefit', 0.1191, 0.1191),
(7, 'Prestasi', 'benefit', 0.1162, 0.1162),
(8, 'Absensi', 'benefit', 0.1889, 0.1889);

-- --------------------------------------------------------

--
-- Struktur dari tabel `nilai`
--

CREATE TABLE `nilai` (
  `id_nilai` int(2) NOT NULL,
  `ket_nilai` varchar(100) NOT NULL,
  `jum_nilai` double NOT NULL,
  `id_kriteria` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `nilai`
--

INSERT INTO `nilai` (`id_nilai`, `ket_nilai`, `jum_nilai`, `id_kriteria`) VALUES
(1, 'Sangat Mampu', 5, 1),
(2, 'Mampu', 4, 1),
(3, 'Cukup Mampu', 3, 1),
(4, 'Tidak Mampu', 2, 1),
(5, 'Sangat Tidak Mampu', 1, 1),
(6, 'Sangat Berat', 5, 2),
(7, 'Berat', 4, 2),
(8, 'Cukup berat', 3, 2),
(9, 'Ringan', 2, 2),
(10, 'Tidak Melanggar', 1, 2),
(11, 'Sangat Disiplin', 5, 3),
(12, 'Disiplin', 4, 3),
(13, 'Cukup Disiplin', 3, 3),
(14, 'Tidak Disiplin', 2, 3),
(15, 'Sangat Tidak Disiplin', 1, 3),
(16, 'Sangat Aktif', 5, 4),
(17, 'Aktif', 4, 4),
(18, 'Cukup Aktif', 3, 4),
(19, 'Tidak Aktif', 2, 4),
(20, 'Sangat Tidak Aktif', 1, 4),
(21, 'Sangat Baik', 5, 5),
(22, 'Baik', 4, 5),
(23, 'Cukup Baik', 3, 5),
(24, 'Tidak Baik', 2, 5),
(25, 'Sangat Tidak Baik', 1, 5),
(26, 'Sangat Mampu', 5, 6),
(27, 'Mampu', 4, 6),
(28, 'Cukup Mampu', 3, 6),
(29, 'Tidak Mampu', 2, 6),
(30, 'Sangat Tidak Mampu', 1, 6),
(31, 'Sangat Baik', 5, 7),
(32, 'Baik', 4, 7),
(33, 'Cukup Baik', 3, 7),
(34, 'Tidak Baik', 2, 7),
(35, 'Sangat Tidak Baik', 1, 7),
(36, 'Sangat Rajin', 5, 8),
(37, 'Rajin', 4, 8),
(38, 'Cukup Rajin', 3, 8),
(39, 'Tidak Rajin', 2, 8),
(40, 'Sangat Tidak Rajin', 1, 8);

-- --------------------------------------------------------

--
-- Struktur dari tabel `rangking`
--

CREATE TABLE `rangking` (
  `kode` int(5) NOT NULL,
  `id_kriteria` int(5) NOT NULL,
  `nilai_rangking` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `rangking`
--

INSERT INTO `rangking` (`kode`, `id_kriteria`, `nilai_rangking`) VALUES
(1, 1, 3),
(1, 2, 1),
(1, 3, 5),
(1, 4, 5),
(1, 5, 4),
(1, 6, 3),
(1, 7, 1),
(1, 8, 5),
(2, 1, 3),
(2, 2, 1),
(2, 3, 5),
(2, 4, 1),
(2, 5, 4),
(2, 6, 3),
(2, 7, 1),
(2, 8, 5),
(3, 1, 3),
(3, 2, 1),
(3, 3, 5),
(3, 4, 4),
(3, 5, 4),
(3, 6, 3),
(3, 7, 1),
(3, 8, 5),
(4, 1, 3),
(4, 2, 1),
(4, 3, 5),
(4, 4, 1),
(4, 5, 4),
(4, 6, 3),
(4, 7, 1),
(4, 8, 5),
(5, 1, 3),
(5, 2, 1),
(5, 3, 4),
(5, 4, 4),
(5, 5, 4),
(5, 6, 3),
(5, 7, 1),
(5, 8, 5),
(6, 1, 3),
(6, 2, 1),
(6, 3, 5),
(6, 4, 1),
(6, 5, 3),
(6, 6, 3),
(6, 7, 1),
(6, 8, 5),
(7, 1, 4),
(7, 2, 1),
(7, 3, 5),
(7, 4, 1),
(7, 5, 3),
(7, 6, 3),
(7, 7, 1),
(7, 8, 5),
(8, 1, 4),
(8, 2, 1),
(8, 3, 5),
(8, 4, 1),
(8, 5, 3),
(8, 6, 3),
(8, 7, 1),
(8, 8, 5),
(9, 1, 4),
(9, 2, 1),
(9, 3, 4),
(9, 4, 4),
(9, 5, 3),
(9, 6, 3),
(9, 7, 1),
(9, 8, 5),
(10, 1, 4),
(10, 2, 1),
(10, 3, 4),
(10, 4, 1),
(10, 5, 3),
(10, 6, 3),
(10, 7, 1),
(10, 8, 5),
(11, 1, 3),
(11, 2, 1),
(11, 3, 5),
(11, 4, 1),
(11, 5, 4),
(11, 6, 3),
(11, 7, 1),
(11, 8, 5),
(12, 1, 3),
(12, 2, 1),
(12, 3, 4),
(12, 4, 1),
(12, 5, 4),
(12, 6, 3),
(12, 7, 1),
(12, 8, 5),
(13, 1, 3),
(13, 2, 1),
(13, 3, 5),
(13, 4, 1),
(13, 5, 3),
(13, 6, 3),
(13, 7, 1),
(13, 8, 5),
(14, 1, 3),
(14, 2, 1),
(14, 3, 5),
(14, 4, 1),
(14, 5, 3),
(14, 6, 3),
(14, 7, 1),
(14, 8, 4),
(15, 1, 3),
(15, 2, 1),
(15, 3, 5),
(15, 4, 1),
(15, 5, 4),
(15, 6, 3),
(15, 7, 1),
(15, 8, 5),
(16, 1, 3),
(16, 2, 1),
(16, 3, 5),
(16, 4, 1),
(16, 5, 3),
(16, 6, 3),
(16, 7, 1),
(16, 8, 5),
(17, 1, 3),
(17, 2, 1),
(17, 3, 5),
(17, 4, 1),
(17, 5, 3),
(17, 6, 3),
(17, 7, 1),
(17, 8, 4),
(18, 1, 3),
(18, 2, 1),
(18, 3, 4),
(18, 4, 1),
(18, 5, 3),
(18, 6, 3),
(18, 7, 1),
(18, 8, 5),
(19, 1, 3),
(19, 2, 1),
(19, 3, 5),
(19, 4, 1),
(19, 5, 3),
(19, 6, 3),
(19, 7, 1),
(19, 8, 5),
(20, 1, 3),
(20, 2, 1),
(20, 3, 5),
(20, 4, 1),
(20, 5, 3),
(20, 6, 3),
(20, 7, 1),
(20, 8, 5),
(21, 1, 4),
(21, 2, 1),
(21, 3, 5),
(21, 4, 3),
(21, 5, 3),
(21, 6, 3),
(21, 7, 1),
(21, 8, 5),
(22, 1, 4),
(22, 2, 1),
(22, 3, 5),
(22, 4, 1),
(22, 5, 4),
(22, 6, 3),
(22, 7, 1),
(22, 8, 5),
(23, 1, 4),
(23, 2, 1),
(23, 3, 4),
(23, 4, 1),
(23, 5, 3),
(23, 6, 3),
(23, 7, 1),
(23, 8, 5),
(24, 1, 4),
(24, 2, 1),
(24, 3, 4),
(24, 4, 1),
(24, 5, 4),
(24, 6, 3),
(24, 7, 1),
(24, 8, 5),
(25, 1, 4),
(25, 2, 1),
(25, 3, 5),
(25, 4, 3),
(25, 5, 3),
(25, 6, 3),
(25, 7, 1),
(25, 8, 5),
(26, 1, 4),
(26, 2, 1),
(26, 3, 5),
(26, 4, 5),
(26, 5, 3),
(26, 6, 3),
(26, 7, 1),
(26, 8, 5),
(27, 1, 3),
(27, 2, 1),
(27, 3, 5),
(27, 4, 3),
(27, 5, 3),
(27, 6, 3),
(27, 7, 1),
(27, 8, 5),
(28, 1, 3),
(28, 2, 1),
(28, 3, 4),
(28, 4, 4),
(28, 5, 3),
(28, 6, 3),
(28, 7, 1),
(28, 8, 5),
(29, 1, 3),
(29, 2, 1),
(29, 3, 4),
(29, 4, 1),
(29, 5, 3),
(29, 6, 3),
(29, 7, 1),
(29, 8, 4),
(30, 1, 3),
(30, 2, 1),
(30, 3, 4),
(30, 4, 1),
(30, 5, 2),
(30, 6, 3),
(30, 7, 1),
(30, 8, 3),
(31, 1, 3),
(31, 2, 1),
(31, 3, 5),
(31, 4, 3),
(31, 5, 3),
(31, 6, 3),
(31, 7, 1),
(31, 8, 5),
(32, 1, 3),
(32, 2, 1),
(32, 3, 4),
(32, 4, 1),
(32, 5, 3),
(32, 6, 3),
(32, 7, 1),
(32, 8, 5),
(33, 1, 3),
(33, 2, 1),
(33, 3, 4),
(33, 4, 1),
(33, 5, 3),
(33, 6, 3),
(33, 7, 1),
(33, 8, 4),
(34, 1, 3),
(34, 2, 1),
(34, 3, 4),
(34, 4, 1),
(34, 5, 3),
(34, 6, 3),
(34, 7, 1),
(34, 8, 3),
(35, 1, 3),
(35, 2, 1),
(35, 3, 4),
(35, 4, 1),
(35, 5, 3),
(35, 6, 3),
(35, 7, 1),
(35, 8, 5),
(36, 1, 3),
(36, 2, 1),
(36, 3, 4),
(36, 4, 5),
(36, 5, 3),
(36, 6, 3),
(36, 7, 1),
(36, 8, 5),
(37, 1, 3),
(37, 2, 1),
(37, 3, 4),
(37, 4, 1),
(37, 5, 3),
(37, 6, 3),
(37, 7, 1),
(37, 8, 5),
(38, 1, 3),
(38, 2, 1),
(38, 3, 4),
(38, 4, 5),
(38, 5, 3),
(38, 6, 3),
(38, 7, 1),
(38, 8, 4),
(39, 1, 3),
(39, 2, 1),
(39, 3, 4),
(39, 4, 1),
(39, 5, 3),
(39, 6, 3),
(39, 7, 1),
(39, 8, 4),
(40, 1, 3),
(40, 2, 1),
(40, 3, 4),
(40, 4, 1),
(40, 5, 3),
(40, 6, 3),
(40, 7, 1),
(40, 8, 1),
(41, 1, 3),
(41, 2, 1),
(41, 3, 5),
(41, 4, 5),
(41, 5, 3),
(41, 6, 3),
(41, 7, 1),
(41, 8, 5),
(42, 1, 3),
(42, 2, 1),
(42, 3, 5),
(42, 4, 1),
(42, 5, 3),
(42, 6, 3),
(42, 7, 1),
(42, 8, 5),
(43, 1, 3),
(43, 2, 1),
(43, 3, 4),
(43, 4, 1),
(43, 5, 2),
(43, 6, 3),
(43, 7, 1),
(43, 8, 3),
(44, 1, 3),
(44, 2, 1),
(44, 3, 4),
(44, 4, 1),
(44, 5, 3),
(44, 6, 3),
(44, 7, 1),
(44, 8, 4),
(45, 1, 3),
(45, 2, 1),
(45, 3, 5),
(45, 4, 1),
(45, 5, 2),
(45, 6, 3),
(45, 7, 1),
(45, 8, 5),
(46, 1, 3),
(46, 2, 1),
(46, 3, 4),
(46, 4, 1),
(46, 5, 3),
(46, 6, 3),
(46, 7, 1),
(46, 8, 5),
(47, 1, 3),
(47, 2, 1),
(47, 3, 4),
(47, 4, 1),
(47, 5, 3),
(47, 6, 3),
(47, 7, 1),
(47, 8, 5),
(48, 1, 3),
(48, 2, 1),
(48, 3, 3),
(48, 4, 3),
(48, 5, 2),
(48, 6, 3),
(48, 7, 1),
(48, 8, 5),
(49, 1, 3),
(49, 2, 1),
(49, 3, 3),
(49, 4, 1),
(49, 5, 3),
(49, 6, 3),
(49, 7, 1),
(49, 8, 3),
(50, 1, 2),
(50, 2, 1),
(50, 3, 5),
(50, 4, 1),
(50, 5, 2),
(50, 6, 3),
(50, 7, 1),
(50, 8, 5),
(51, 1, 3),
(51, 2, 1),
(51, 3, 4),
(51, 4, 5),
(51, 5, 3),
(51, 6, 3),
(51, 7, 1),
(51, 8, 5),
(52, 1, 3),
(52, 2, 1),
(52, 3, 3),
(52, 4, 1),
(52, 5, 2),
(52, 6, 3),
(52, 7, 1),
(52, 8, 5),
(53, 1, 3),
(53, 2, 1),
(53, 3, 4),
(53, 4, 1),
(53, 5, 3),
(53, 6, 3),
(53, 7, 5),
(53, 8, 5),
(54, 1, 3),
(54, 2, 1),
(54, 3, 4),
(54, 4, 5),
(54, 5, 2),
(54, 6, 3),
(54, 7, 5),
(54, 8, 5),
(55, 1, 3),
(55, 2, 1),
(55, 3, 4),
(55, 4, 5),
(55, 5, 3),
(55, 6, 3),
(55, 7, 1),
(55, 8, 5),
(56, 1, 3),
(56, 2, 1),
(56, 3, 3),
(56, 4, 5),
(56, 5, 3),
(56, 6, 3),
(56, 7, 1),
(56, 8, 5),
(57, 1, 3),
(57, 2, 1),
(57, 3, 3),
(57, 4, 3),
(57, 5, 3),
(57, 6, 3),
(57, 7, 1),
(57, 8, 5),
(58, 1, 3),
(58, 2, 1),
(58, 3, 3),
(58, 4, 1),
(58, 5, 2),
(58, 6, 3),
(58, 7, 1),
(58, 8, 5),
(59, 1, 3),
(59, 2, 1),
(59, 3, 3),
(59, 4, 1),
(59, 5, 3),
(59, 6, 3),
(59, 7, 1),
(59, 8, 5),
(60, 1, 3),
(60, 2, 1),
(60, 3, 3),
(60, 4, 3),
(60, 5, 3),
(60, 6, 3),
(60, 7, 1),
(60, 8, 5),
(61, 1, 3),
(61, 2, 1),
(61, 3, 3),
(61, 4, 1),
(61, 5, 2),
(61, 6, 3),
(61, 7, 1),
(61, 8, 5),
(62, 1, 3),
(62, 2, 1),
(62, 3, 3),
(62, 4, 3),
(62, 5, 2),
(62, 6, 3),
(62, 7, 1),
(62, 8, 5),
(63, 1, 3),
(63, 2, 1),
(63, 3, 3),
(63, 4, 1),
(63, 5, 2),
(63, 6, 3),
(63, 7, 1),
(63, 8, 5),
(64, 1, 3),
(64, 2, 1),
(64, 3, 3),
(64, 4, 5),
(64, 5, 2),
(64, 6, 3),
(64, 7, 1),
(64, 8, 5),
(65, 1, 3),
(65, 2, 1),
(65, 3, 3),
(65, 4, 1),
(65, 5, 2),
(65, 6, 3),
(65, 7, 1),
(65, 8, 5),
(66, 1, 3),
(66, 2, 1),
(66, 3, 3),
(66, 4, 1),
(66, 5, 2),
(66, 6, 3),
(66, 7, 1),
(66, 8, 5),
(67, 1, 3),
(67, 2, 1),
(67, 3, 3),
(67, 4, 1),
(67, 5, 3),
(67, 6, 3),
(67, 7, 1),
(67, 8, 5),
(68, 1, 3),
(68, 2, 1),
(68, 3, 3),
(68, 4, 1),
(68, 5, 2),
(68, 6, 3),
(68, 7, 1),
(68, 8, 5),
(69, 1, 3),
(69, 2, 1),
(69, 3, 3),
(69, 4, 1),
(69, 5, 2),
(69, 6, 3),
(69, 7, 1),
(69, 8, 3),
(70, 1, 3),
(70, 2, 1),
(70, 3, 3),
(70, 4, 5),
(70, 5, 2),
(70, 6, 3),
(70, 7, 1),
(70, 8, 5),
(71, 1, 3),
(71, 2, 1),
(71, 3, 4),
(71, 4, 5),
(71, 5, 3),
(71, 6, 3),
(71, 7, 1),
(71, 8, 5),
(72, 1, 3),
(72, 2, 1),
(72, 3, 4),
(72, 4, 3),
(72, 5, 3),
(72, 6, 3),
(72, 7, 1),
(72, 8, 5),
(73, 1, 3),
(73, 2, 1),
(73, 3, 5),
(73, 4, 1),
(73, 5, 3),
(73, 6, 3),
(73, 7, 1),
(73, 8, 5),
(74, 1, 3),
(74, 2, 1),
(74, 3, 4),
(74, 4, 3),
(74, 5, 3),
(74, 6, 3),
(74, 7, 1),
(74, 8, 4),
(75, 1, 3),
(75, 2, 1),
(75, 3, 4),
(75, 4, 1),
(75, 5, 3),
(75, 6, 3),
(75, 7, 1),
(75, 8, 5),
(76, 1, 3),
(76, 2, 1),
(76, 3, 5),
(76, 4, 1),
(76, 5, 3),
(76, 6, 3),
(76, 7, 1),
(76, 8, 5),
(77, 1, 3),
(77, 2, 1),
(77, 3, 5),
(77, 4, 1),
(77, 5, 3),
(77, 6, 3),
(77, 7, 1),
(77, 8, 5),
(78, 1, 3),
(78, 2, 2),
(78, 3, 4),
(78, 4, 1),
(78, 5, 3),
(78, 6, 3),
(78, 7, 1),
(78, 8, 5),
(79, 1, 3),
(79, 2, 1),
(79, 3, 5),
(79, 4, 1),
(79, 5, 3),
(79, 6, 2),
(79, 7, 1),
(79, 8, 5),
(80, 1, 3),
(80, 2, 1),
(80, 3, 5),
(80, 4, 1),
(80, 5, 3),
(80, 6, 3),
(80, 7, 1),
(80, 8, 5),
(81, 1, 3),
(81, 2, 1),
(81, 3, 5),
(81, 4, 3),
(81, 5, 3),
(81, 6, 3),
(81, 7, 1),
(81, 8, 5),
(82, 1, 3),
(82, 2, 1),
(82, 3, 4),
(82, 4, 3),
(82, 5, 3),
(82, 6, 3),
(82, 7, 1),
(82, 8, 5),
(83, 1, 3),
(83, 2, 1),
(83, 3, 4),
(83, 4, 1),
(83, 5, 3),
(83, 6, 3),
(83, 7, 1),
(83, 8, 5),
(84, 1, 3),
(84, 2, 1),
(84, 3, 4),
(84, 4, 1),
(84, 5, 3),
(84, 6, 3),
(84, 7, 1),
(84, 8, 5),
(85, 1, 3),
(85, 2, 1),
(85, 3, 4),
(85, 4, 5),
(85, 5, 3),
(85, 6, 3),
(85, 7, 1),
(85, 8, 5),
(86, 1, 3),
(86, 2, 1),
(86, 3, 4),
(86, 4, 1),
(86, 5, 2),
(86, 6, 3),
(86, 7, 1),
(86, 8, 2),
(87, 1, 3),
(87, 2, 1),
(87, 3, 5),
(87, 4, 1),
(87, 5, 3),
(87, 6, 3),
(87, 7, 1),
(87, 8, 2),
(88, 1, 3),
(88, 2, 1),
(88, 3, 4),
(88, 4, 1),
(88, 5, 3),
(88, 6, 3),
(88, 7, 1),
(88, 8, 3),
(89, 1, 3),
(89, 2, 1),
(89, 3, 5),
(89, 4, 1),
(89, 5, 3),
(89, 6, 3),
(89, 7, 1),
(89, 8, 5),
(90, 1, 3),
(90, 2, 1),
(90, 3, 4),
(90, 4, 1),
(90, 5, 3),
(90, 6, 3),
(90, 7, 1),
(90, 8, 3),
(91, 1, 3),
(91, 2, 1),
(91, 3, 4),
(91, 4, 1),
(91, 5, 3),
(91, 6, 3),
(91, 7, 1),
(91, 8, 4),
(92, 1, 3),
(92, 2, 1),
(92, 3, 4),
(92, 4, 1),
(92, 5, 3),
(92, 6, 3),
(92, 7, 1),
(92, 8, 1),
(93, 1, 3),
(93, 2, 1),
(93, 3, 4),
(93, 4, 1),
(93, 5, 3),
(93, 6, 3),
(93, 7, 1),
(93, 8, 3),
(94, 1, 3),
(94, 2, 1),
(94, 3, 4),
(94, 4, 1),
(94, 5, 3),
(94, 6, 3),
(94, 7, 1),
(94, 8, 1),
(95, 1, 3),
(95, 2, 1),
(95, 3, 4),
(95, 4, 1),
(95, 5, 3),
(95, 6, 3),
(95, 7, 1),
(95, 8, 5),
(96, 1, 3),
(96, 2, 1),
(96, 3, 4),
(96, 4, 1),
(96, 5, 3),
(96, 6, 3),
(96, 7, 1),
(96, 8, 5),
(97, 1, 3),
(97, 2, 1),
(97, 3, 5),
(97, 4, 5),
(97, 5, 3),
(97, 6, 3),
(97, 7, 1),
(97, 8, 5),
(98, 1, 3),
(98, 2, 1),
(98, 3, 4),
(98, 4, 1),
(98, 5, 3),
(98, 6, 3),
(98, 7, 1),
(98, 8, 5),
(99, 1, 3),
(99, 2, 1),
(99, 3, 4),
(99, 4, 1),
(99, 5, 3),
(99, 6, 3),
(99, 7, 1),
(99, 8, 5),
(100, 1, 3),
(100, 2, 1),
(100, 3, 4),
(100, 4, 3),
(100, 5, 3),
(100, 6, 3),
(100, 7, 1),
(100, 8, 4),
(101, 1, 3),
(101, 2, 1),
(101, 3, 5),
(101, 4, 3),
(101, 5, 3),
(101, 6, 3),
(101, 7, 1),
(101, 8, 4),
(102, 1, 3),
(102, 2, 1),
(102, 3, 4),
(102, 4, 3),
(102, 5, 3),
(102, 6, 3),
(102, 7, 1),
(102, 8, 5),
(103, 1, 3),
(103, 2, 1),
(103, 3, 4),
(103, 4, 1),
(103, 5, 3),
(103, 6, 3),
(103, 7, 1),
(103, 8, 4),
(104, 1, 3),
(104, 2, 1),
(104, 3, 4),
(104, 4, 1),
(104, 5, 3),
(104, 6, 3),
(104, 7, 1),
(104, 8, 5),
(105, 1, 3),
(105, 2, 1),
(105, 3, 4),
(105, 4, 1),
(105, 5, 3),
(105, 6, 3),
(105, 7, 1),
(105, 8, 5);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `alternatif`
--
ALTER TABLE `alternatif`
  ADD PRIMARY KEY (`kode`);

--
-- Indeks untuk tabel `hasil`
--
ALTER TABLE `hasil`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `hasil_history`
--
ALTER TABLE `hasil_history`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `kriteria`
--
ALTER TABLE `kriteria`
  ADD PRIMARY KEY (`id_kriteria`);

--
-- Indeks untuk tabel `nilai`
--
ALTER TABLE `nilai`
  ADD PRIMARY KEY (`id_nilai`);

--
-- Indeks untuk tabel `rangking`
--
ALTER TABLE `rangking`
  ADD PRIMARY KEY (`kode`,`id_kriteria`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `alternatif`
--
ALTER TABLE `alternatif`
  MODIFY `kode` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=106;

--
-- AUTO_INCREMENT untuk tabel `hasil`
--
ALTER TABLE `hasil`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=106;

--
-- AUTO_INCREMENT untuk tabel `hasil_history`
--
ALTER TABLE `hasil_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT untuk tabel `kriteria`
--
ALTER TABLE `kriteria`
  MODIFY `id_kriteria` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `nilai`
--
ALTER TABLE `nilai`
  MODIFY `id_nilai` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT untuk tabel `rangking`
--
ALTER TABLE `rangking`
  MODIFY `kode` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=106;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

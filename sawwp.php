?>
<!DOCTYPE html>
<html>
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Hasil Ranking</title>
		<script type="text/javascript" charset="utf8" src="js/jquery.min.js"></script>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
		<link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">
    <!-- Custom styles for this template -->
		<link rel="stylesheet" type="text/css" href="css/jquery.dataTables.min.css" rel="stylesheet">

		<script type="text/javascript" charset="utf8" src="js/jquery.dataTables.min.js"></script>
  </head>

<body>
<nav style="background: green;" class="navbar navbar-inverse navbar-fixed-top bg-dark">
	  <div class="container" style="font-color: #000">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
		 
		  <a class="navbar-brand" href="home.php" style="color: #ffffff">HOME</a>
		</div>

		<span class="navbar-toggler-icon"></span>
        	<div class="collapse navbar-collapse" id="navbarResponsive">
         <ul class="nav navbar-nav">
         	<li class="nav-item active">
              <a class="nav-link" href="home.php" style="color: #ffffff">Metode
                <span class="sr-only">(current)</span>
              </a>
            </li>
         	<li class="nav-item active">
              <a class="nav-link" href="kriteria.php" style="color: #ffffff">Kriteria
                <span class="sr-only">(current)</span>
              </a>
            </li>

             <li class="nav-item active">
              <a class="nav-link" href="alternatif.php" style="color: #ffffff">Alternatif
                <span class="sr-only">(current)</span>
              </a>
            </li>
		  </ul>
		</div><!-- /.navbar-collapse -->

		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		  <ul class="nav navbar-nav navbar-right">
			
			  </ul>
			</li>
		  </ul>
		</div><!-- /.navbar-collapse -->
	  </div><!-- /.container-fluid -->
	</nav>

<body style="background-color: lightgreen;"> </body>
<div class="container" style="padding-top: 60px">
	<div class="row">
		<div class="col-lg-6" style="padding-right:50px">
		<div class="tabel-saw">
			<div class="row">
				<div class="col-md-8">
					<h2>Hasil Rangking SAW</h2>
				</div>
				<div class="text-center col-md-3">
					<button type="button" class="btn btn-primary" style="background:darkred" onclick="window.open('detail_saw.php')">Lihat Detail SAW</button>
				</div>
				<div class="col-md-1"></div>
			</div>
			<br>
			<table class="table table-hover table-bordered table-sm table-secondary " style="border: 10px solid: #ddd !important;" id="table2">
			  <thead style="background: darkgreen; color:white">
			    <tr>
				  <th scope="col" style= "text-align: center">No</th>
			      <th scope="col" style= "text-align: center">Nama Siswa</th>
			      <th scope="col" style= "text-align: center">Hasil</th>
			    </tr>
			  </thead>
			  <tbody>
            <?php
            include_once 'connection.php';
                $sql = "SELECT `kode`,`namalengkap` FROM alternatif";
                $result = $conn->query($sql);
                while($alternatif = $result->fetch_assoc()) {
                    $hasil = 0;
                    $id = $alternatif['kode'];
                    $sqlKrit = "SELECT k.`bobot_kriteria`, r.`id_kriteria`,k.`tipe_kriteria`, r.`nilai_rangking` FROM `alternatif` a JOIN `rangking` r ON a.`kode` = r.`kode` JOIN `kriteria` k ON r.`id_kriteria` = k.`id_kriteria` JOIN `nilai` n ON r.`id_kriteria` = n.`id_kriteria` WHERE r.`nilai_rangking` = n.`jum_nilai` AND a.`kode` = '$id'";
                    $result2 = $conn->query($sqlKrit);
            ?>
                <tr style="background: #E0E0E0">
                    <td>
                        <?= $alternatif['kode'] ?>
                    </td>
                    <td>
                        <?= $alternatif['namalengkap'] ?>
                    </td>
                    <?php
                        while($kriteria = $result2->fetch_assoc()){
                    ?>
                 
                        <?php
							$id = $kriteria['id_kriteria'];
                            if ($kriteria['tipe_kriteria'] == "cost"){
                                $idKriteria = $kriteria['id_kriteria'];
                                $sqlMin = "SELECT MIN(`nilai_rangking`) as 'nilai' FROM rangking where `id_kriteria` = '$idKriteria' LIMIT 1";
                                $exeMin = $conn->query($sqlMin);
                                $min = mysqli_fetch_assoc($exeMin);
                                $nor = ($min['nilai']/$kriteria['nilai_rangking'])*$kriteria['bobot_kriteria'];
                               
                            } else {
                                $idKriteria = $kriteria['id_kriteria'];
                                $sqlMax = "SELECT MAX(`nilai_rangking`) as 'nilai' FROM rangking where `id_kriteria` = '$idKriteria' LIMIT 1";
                                $exeMax = $conn->query($sqlMax);
                                $max = mysqli_fetch_assoc($exeMax);
                                $nor = ($kriteria['nilai_rangking']/$max['nilai'])*$kriteria['bobot_kriteria'];
                               
                            }
														$hasil+=$nor;
                            // echo $kriteria['nilai_rangking']; 
                        ?>
                  
										<?php 
									}
									
									$sqlupdate = "UPDATE `hasil` SET `hasil_saw` = '$hasil' WHERE `kode` = ".$alternatif['kode'];
									$i = $conn->query($sqlupdate);
									?>
                    <td><?= number_format($hasil,5); ?></td>
                </tr>
            <?php
                }
            ?>
        </tbody>
			</table>
			
			
		</div>
			
		</div>
		<div class="col-lg-6">
			<div class="tabel-saw">
				<div class="row">
					<div class="col-md-8">
						<h2>Hasil Rangking WP </h2>
					</div>
					<div class="text-center col-md-3">
						<button type="button" class="btn btn-primary" style="background:darkred" onclick="window.open('detail_wp.php')">Lihat Detail WP</button>
					</div>
					<div class="col-md-1"></div>
				</div>
				<br>
				<table class="table table-striped table-bordered" style="border: 10px solid: #ddd !important;" id="table1">
			  <thead style="background: darkgreen; color:white">
			    <tr>
					<th scope="col" style= "text-align: center">No</th>
			      	<th scope="col" style= "text-align: center">Nama Siswa</th>
					<th scope="col" style= "text-align: center">Nilai WP</th>
			    </tr>
			  </thead>
				<tbody>
          
				<?php
					$totalS = 0;
					$sql = "SELECT `kode`,`namalengkap` FROM alternatif";
					$result = $conn->query($sql);
					while($alternatif = $result->fetch_assoc()) {
							$id = $alternatif['kode'];
							$sqlKrit = "SELECT r.`id_kriteria`,k.`tipe_kriteria`, r.`nilai_rangking` FROM `alternatif` a JOIN `rangking` r ON a.`kode` = r.`kode` JOIN `kriteria` k ON r.`id_kriteria` = k.`id_kriteria` JOIN `nilai` n ON r.`id_kriteria` = n.`id_kriteria` WHERE r.`nilai_rangking` = n.`jum_nilai` AND a.`kode` = '$id'";
							$result2 = $conn->query($sqlKrit);
							$nilaiS = 1;
							while($kriteria = $result2->fetch_assoc()){
					
									$idKriteria = $kriteria['id_kriteria'];
									$sqlKrit = "SELECT `bobot_kriteria` as 'nilai' FROM kriteria where `id_kriteria` = '$idKriteria' LIMIT 1";
									$exeKrit = $conn->query($sqlKrit);
									$kriteriaPangkat = mysqli_fetch_assoc($exeKrit);
									$pangkat = $kriteriaPangkat['nilai'];		
									if ($kriteria['tipe_kriteria'] == "cost"){
											$pangkat = "-".$kriteriaPangkat['nilai'];
									} 
									$nilaiSAlt = pow($kriteria['nilai_rangking'], $pangkat); 
									// echo $nilaiSAlt;
									$nilaiS *= $nilaiSAlt;  
									
							} 
							$totalS = $totalS + $nilaiS;
						}
            ?>
						<?php
							$sql = "SELECT `kode`,`namalengkap` FROM alternatif";
							$result = $conn->query($sql);
							while($alternatif = $result->fetch_assoc()) {
									$id = $alternatif['kode'];
									$sqlKrit = "SELECT r.`id_kriteria`,k.`tipe_kriteria`, r.`nilai_rangking` FROM `alternatif` a JOIN `rangking` r ON a.`kode` = r.`kode` JOIN `kriteria` k ON r.`id_kriteria` = k.`id_kriteria` JOIN `nilai` n ON r.`id_kriteria` = n.`id_kriteria` WHERE r.`nilai_rangking` = n.`jum_nilai` AND a.`kode` = '$id'";
									$result2 = $conn->query($sqlKrit);
									$nilaiS = 1;
										
            ?>
                <tr style="background: #E0E0E0">
										<td>
                        <?= $alternatif['kode'] ?>
                    </td>
                    
                    <td>
                        <?= $alternatif['namalengkap'] ?>
                    </td>
                    <?php
                        while($kriteria = $result2->fetch_assoc()){
                    
													$idKriteria = $kriteria['id_kriteria'];
														$sqlKrit = "SELECT `bobot_kriteria` as 'nilai' FROM kriteria where `id_kriteria` = '$idKriteria' LIMIT 1";
														$exeKrit = $conn->query($sqlKrit);
														$kriteriaPangkat = mysqli_fetch_assoc($exeKrit);
														$pangkat = $kriteriaPangkat['nilai'];		
                            if ($kriteria['tipe_kriteria'] == "cost"){
                                $pangkat = "-".$kriteriaPangkat['nilai'];
														} 
														$nilaiSAlt = pow($kriteria['nilai_rangking'], $pangkat); 
														// echo $nilaiSAlt;
														$nilaiS *= $nilaiSAlt; 
														$hasil = $nilaiS/$totalS;
														$sqlupdate = "UPDATE `hasil` SET `hasil_wp` = '$hasil' WHERE `kode` = '$id'";
                  					$i = $conn->query($sqlupdate); 
													
                        } ?>
										<td>
												<?= number_format($hasil,5); ?>
										</td>
                </tr>
            <?php
                }
            ?>
        </tbody>
			</table>
			
			  </tbody>
			</table>
		</div>
	</div>
</div>
<br>
<div class="container">
	<div class="row">
			<div class="col-md-3"></div>
				<div class="text-center col-md-6">
				<button type="button" class="btn btn-primary col-md-12" style="background:darkred" onclick="window.open('analisis-sensitifitas.php')">Analisis Sensitivitas</button>

				</div>
			<div class="col-md-3"></div>
	</div>
	<br>
</div>

<script type="text/javascript">
		$(document).ready( function () {
				$('#table1').DataTable();
		} );
	</script>
	<script type="text/javascript">
		$(document).ready( function () {
				$('#table2').DataTable();
		} );
	</script>
		<!-- Footer -->
    <footer>
      <div class="container">
        <div class="row">
             <p class="m-0 text-center text-white">Copyright &copy; Maghfira Izzani Rahmayani || 16650017</p>
          </div>
        </div>
      </div>

 	</footer>
</body>
</html>
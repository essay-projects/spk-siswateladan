<?php

	include_once 'connection.php';

?>
<!DOCTYPE html>
<html>
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Detail WP</title>
		<script type="text/javascript" charset="utf8" src="js/jquery.min.js"></script>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
		<link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">
    <!-- Custom styles for this template -->
		<link rel="stylesheet" type="text/css" href="css/jquery.dataTables.min.css" rel="stylesheet">

		<script type="text/javascript" charset="utf8" src="js/jquery.dataTables.min.js"></script>
  </head>

<body>
<nav style="background:green;" class="navbar navbar-inverse navbar-fixed-top bg-dark">
	  <div class="container" style="font-color: #000">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
		 
		  <a class="navbar-brand" href="index.php" style="color: #ffffff">HOME</a>
		</div>
		  <span class="navbar-toggler-icon"></span>
        	<div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="nav navbar-nav">
            	<li class="nav-item active">
              <a class="nav-link" href="home.php" style="color: #ffffff">Metode
                <span class="sr-only">(current)</span>
              </a>
            </li>
            <li class="nav-item active">
              <a class="nav-link" href="kriteria.php" style="color: #ffffff">Kriteria
                <span class="sr-only">(current)</span>
              </a>
            </li>

             <li class="nav-item active">
              <a class="nav-link" href="alternatif.php" style="color: #ffffff">Alternatif
                <span class="sr-only">(current)</span>
              </a>
            </li>
            <li class="nav-item active">
              <a class="nav-link" href="sawwp.php" style="color: #ffffff">Hasil Perhitungan
                <span class="sr-only">(current)</span>
              </a>
            </li>
		  </ul>
		</div><!-- /.navbar-collapse -->

		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		  <ul class="nav navbar-nav navbar-right">
			
			  </ul>
			</li>
		  </ul>
		</div><!-- /.navbar-collapse -->
	  </div><!-- /.container-fluid -->
	</nav>
<body style="background-color: lightgreen;"> </body>
	<div class="container" style="padding-top: 60px;">
		<div class="tabel-saw" style="text-align: center; color:black">
			<h2>KRITERIA YANG DI INPUT</h2>
			<table class="table table-striped table-bordered" style="border: 10px solid: #ddd !important;">
			  <thead style="background:darkgreen; color:white">
			    <tr>
			      <th scope="col" style="text-align: center">Kode Kriteria</th>
			      <th scope="col" style="text-align: center">Nama Kriteria</th>
			      <th scope="col" style="text-align: center">Jenis</th>
				  <th scope="col" style="text-align: center">Bobot Kriteria</th>
			    </tr>
			  </thead>
			  <tbody >
					<?php
							$sql = "SELECT * FROM kriteria";
							$result = $conn->query($sql);
							while($row = $result->fetch_assoc()) {
					?>
					<tr style="background: #E0E0E0; color:black; text-align: left">
							<td style="text-align: center">
									<?= $row['id_kriteria'] ?>
							</td>
							<td>
									<?= $row['nama_kriteria'] ?>
							</td>
							<td>
									<?= $row['tipe_kriteria'] ?>
							</td>
							<td style="text-align: center">
									<?= $row['bobot_kriteria'] ?>
							</td>
					</tr>
				<?php
						}
            ?>
			  </tbody>
			</table>
		</div>
	</div>

	<div class="container" style="padding-top: 10px; color:black">
		<div class="tabel-saw">
			<h2>DATA ALTERNARTIF</h2>
			<p>Data Siswa Kelas 8 MTs Negeri 5 Cirebon</p>
			<table class="table table-striped table-bordered" style="border: 10px solid: #ddd !important;" id="table1">
			  <thead style="background: darkgreen; color:white">
			    <tr>
				  <th scope="col" style="text-align: center">Kode</th>
			      <th scope="col" style="text-align: center">Nama Lengkap</th>
			      <th scope="col" style="text-align: center">Alamat</th>
			      <th scope="col" style="text-align: center">Angkatan</th>
			      <th scope="col" style="text-align: center">Kelas</th>
			    </tr>
			  </thead>
			  <tbody>
            <?php
                $sql = "SELECT * FROM alternatif";
                $result = $conn->query($sql);
                while($alternatif = $result->fetch_assoc()) {
                    $id = $alternatif['kode'];
            		?>
                <tr style="background: #E0E0E0;color:black">
                    <td style="text-align: center">
                        <?= $alternatif['kode'] ?>
                    </td>
                    <td>
                        <?= $alternatif['namalengkap'] ?>
                    </td>
                    <td>
												<?= $alternatif['alamat'] ?>
                    </td>
                    <td style="text-align: center">
												<?= $alternatif['angkatan'] ?>
                    </td>
                    <td style="text-align: center">
												<?= $alternatif['kelas'] ?>
                    </td>
                </tr>
            <?php
                }
            ?>
        </tbody>
			</table>
		</div>
	</div>

	<div class="container" style="padding-top: 10px; color:black">
		<div class="tabel-saw">
			<h2>NILAI KRITERIA SISWA</h2>
			<p>Data Nilai Siswa</p>
			<table class="table table-striped table-bordered" style="border: 10px solid: #ddd !important;" id="table2">
			  <thead style="background:darkgreen; color:white">
			    <tr>
				  <th scope="col" style="text-align: center">No</th>
			      <th scope="col" style="text-align: center">Nama Lengkap</th>
			      <th scope="col" style="text-align: center">Nilai Skill</th>
			      <th scope="col" style="text-align: center">Pelanggaran</th>
			      <th scope="col" style="text-align: center">Kedisiplinan</th>
			      <th scope="col" style="text-align: center">Ekstrakurikuler</th>
			      <th scope="col" style="text-align: center">Baca Tulis Qur’an (BTQ)</th>
			      <th scope="col" style="text-align: center">Pengetahuan Siswa </th>
			      <th scope="col" style="text-align: center">Prestasi</th>
                  <th scope="col" style="text-align: center">Absensi</th>
                  
			    </tr>
			  </thead>
			  <tbody>
            <?php
                $sql = "SELECT `kode`,`namalengkap` FROM alternatif";
                $result = $conn->query($sql);
                while($alternatif = $result->fetch_assoc()) {
                    $id = $alternatif['kode'];
                    $sqlKrit = "SELECT a.`namalengkap`, k.`nama_kriteria`, r.`nilai_rangking` , n.`ket_nilai`FROM `alternatif` a JOIN `rangking` r ON a.`kode` = r.`kode` JOIN `kriteria` k ON r.`id_kriteria` = k.`id_kriteria` JOIN `nilai` n ON r.`id_kriteria` = n.`id_kriteria` WHERE r.`nilai_rangking` = n.`jum_nilai` AND a.`kode` = '$id'";
                    $result2 = $conn->query($sqlKrit);
            ?>
                <tr style="background: #E0E0E0; color:black;">
                    <td style="text-align: center">
                        <?= $alternatif['kode'] ?>
                    </td>
                    <td>
                        <?= $alternatif['namalengkap'] ?>
                    </td>
                    <?php
                        while($kriteria = $result2->fetch_assoc()){
                    ?>
                    <td>
                            <?php echo $kriteria['ket_nilai'] ?>
                    </td>
                    <?php } ?>
                </tr>
            <?php
                }
            ?>
        </tbody>
			</table>
		</div>
	</div>
		
	<div class="container" style="padding-top: 10px; color:black">
		<div class="tabel-saw">
			<h2>MATRIKS NILAI SISWA</h2>
			<p>Data Nilai Siswa</p>
			<table class="table table-striped table-bordered" style="border: 10px solid: #ddd !important;" id="table3">
			  <thead style="background:darkgreen; color:white">
			    <tr>
				  <th scope="col" style="text-align: center" >Kode</th>
			      <th scope="col" style="text-align: center">Nama Lengkap</th>
                  <th scope="col" style="text-align: center">Nilai Skill</th>
                  <th scope="col" style="text-align: center">Pelanggaran</th>
                  <th scope="col" style="text-align: center">Kedisiplinan</th>
                  <th scope="col" style="text-align: center">Ekstrakurikuler</th>
                  <th scope="col" style="text-align: center">Baca Tulis Qur’an (BTQ)</th>
                  <th scope="col" style="text-align: center">Pengetahuan Siswa </th>
                  <th scope="col" style="text-align: center">Prestasi</th>
                  <th scope="col" style="text-align: center">Absensi</th>
                  
			    </tr>
			  </thead>
			  <tbody>
            <?php
                $sql = "SELECT `kode`,`namalengkap` FROM alternatif";
                $result = $conn->query($sql);
                while($alternatif = $result->fetch_assoc()) {
                    $id = $alternatif['kode'];
                    $sqlKrit = "SELECT a.`namalengkap`, k.`nama_kriteria`, r.`nilai_rangking` , n.`ket_nilai`FROM `alternatif` a JOIN `rangking` r ON a.`kode` = r.`kode` JOIN `kriteria` k ON r.`id_kriteria` = k.`id_kriteria` JOIN `nilai` n ON r.`id_kriteria` = n.`id_kriteria` WHERE r.`nilai_rangking` = n.`jum_nilai` AND a.`kode` = '$id'";
                    $result2 = $conn->query($sqlKrit);
            ?>
                <tr style="background: #E0E0E0; color:black;">
                    <td style="text-align: center">
                        <?= $alternatif['kode'] ?>
                    </td>
                    <td>
                        <?= $alternatif['namalengkap'] ?>
                    </td>
                    <?php
                        while($kriteria = $result2->fetch_assoc()){
                    ?>
                    <td style="text-align: center">
                            <?php echo $kriteria['nilai_rangking'] ?>
                    </td>
                    <?php } ?>
                </tr>
            <?php
                }
            ?>
        </tbody>
			</table>
		</div>
	</div>
		
	<div class="container" style="padding-top: 10px; color:black">
		<div class="tabel-saw">
			<h2>Nilai Vektor S Siswa</h2>
			
			
			<table class="table table-striped table-bordered" style="border: 10px solid: #ddd !important;" id="table4">
			  <thead style="background: darkgreen; color:white">
			    <tr>
					<th scope="col" style="text-align: center">Kode</th>
					<th scope="col" style="text-align: center">Vektor S</th>
			      	<th scope="col" style="text-align: center">Nama Siswa</th>
					<th scope="col" style="text-align: center">Nilai Vektor S</th>
			    </tr>
			  </thead>
				<tbody>
            <?php
							$totalS = 0;
							$sql = "SELECT `kode`,`namalengkap` FROM alternatif";
							$result = $conn->query($sql);
							while($alternatif = $result->fetch_assoc()) {
									$id = $alternatif['kode'];
									$sqlKrit = "SELECT r.`id_kriteria`,k.`tipe_kriteria`, r.`nilai_rangking` FROM `alternatif` a JOIN `rangking` r ON a.`kode` = r.`kode` JOIN `kriteria` k ON r.`id_kriteria` = k.`id_kriteria` JOIN `nilai` n ON r.`id_kriteria` = n.`id_kriteria` WHERE r.`nilai_rangking` = n.`jum_nilai` AND a.`kode` = '$id'";
									$result2 = $conn->query($sqlKrit);
									$nilaiS = 1;
										
            ?>
                <tr style="background: #E0E0E0; color:black">
					<td style="text-align: center">
                        <?= $alternatif['kode'] ?>
                    </td>
                    <td style="text-align: center">
                        <?= "S".$alternatif['kode'] ?>
                    </td>
                    <td>
                        <?= $alternatif['namalengkap'] ?>
                    </td>
                    <?php
                        while($kriteria = $result2->fetch_assoc()){

							$idKriteria = $kriteria['id_kriteria'];
							$sqlKrit = "SELECT `bobot_kriteria` as 'nilai' FROM kriteria where `id_kriteria` = '$idKriteria' LIMIT 1";
							$exeKrit = $conn->query($sqlKrit);
							$kriteriaPangkat = mysqli_fetch_assoc($exeKrit);
							$pangkat = $kriteriaPangkat['nilai'];		
							//matriks S = nilai rangking kriteria pangkat bobot, jika kriteria cost = nilai rangking pangkat minus bobot
	                        if ($kriteria['tipe_kriteria'] == "cost"){
	                            $pangkat = "-".$kriteriaPangkat['nilai'];
							} 
							$nilaiSAlt = pow($kriteria['nilai_rangking'], $pangkat); 
							// echo $nilaiSAlt;
							$nilaiS *= $nilaiSAlt;  
														
                        } ?>
					<td style="text-align: center">
									
							<?php $totalS = $totalS + $nilaiS;
								echo number_format($nilaiS,5);
							?>
					</td>
                </tr>
            <?php
                }
            ?>
        </tbody>
			</table>
		</div>
	</div>

	<div class="container" style="padding-top: 10px; color:black">
			
			<div class="tabel-saw">
				<h2>Nilai Vektor V Siswa</h2>
				
				<table class="table table-striped table-bordered" style="border: 10px solid: #ddd !important;" id="table5">
			  <thead style="background: darkgreen; color:white">
			    <tr>
					<th scope="col" style="text-align: center">Kode</th>
			    	<th scope="col" style="text-align: center">Nama siswa</th>
					<th scope="col" style="text-align: center">Nilai WP</th>
			    </tr>
			  </thead>
				<tbody>
            <?php
				$sql = "SELECT * FROM `alternatif` a JOIN `hasil` h ON a.`kode` = h.`kode`";
				$result = $conn->query($sql);
				$i = 0;
				while($alternatif = $result->fetch_assoc()) {
        	?>
					<tr style="background: #E0E0E0; color:black">
						<td style="text-align: center">
								<?= ++$i; ?>
						</td>
						
						<td>
								<?= $alternatif['namalengkap'] ?>
						</td>
						<td style="text-align: center">
								<?= number_format($alternatif['hasil_wp'],5) ?>
						</td>
                </tr>
            <?php
                }
            ?>
        </tbody>
			</table>
			</div>
	</div>
	


	<div class="container" style="padding-top: 10px; text-align: center; color:black">
	<div class="tabel-saw">
	<h2>HASIL RANKING WEIGHTED PRODUCT</h2>
				
				
				<table class="table table-striped table-bordered" style="border: 10px solid: #ddd !important;" id="table6">
			  <thead style="background: darkgreen; color:white">
			    <tr>
				  <th scope="col" style="text-align: center">No</th>
			      <th scope="col" style="text-align: center">Kode</th>
			      <th scope="col" style="text-align: center">Nama Siswa</th>
				  <th scope="col" style="text-align: center">Nilai WP</th>
			    </tr>
			  </thead>
				<tbody>
            <?php
				$sql = "SELECT * FROM `alternatif` a JOIN `hasil` h ON a.`kode` = h.`kode` ORDER BY h.`hasil_wp` DESC";
				$result = $conn->query($sql);
				$i = 0;
				while($alternatif = $result->fetch_assoc()) {
        	?>
					<tr style="background: #E0E0E0; color:black">
						<td style="text-align: center">
								<?= ++$i; ?>
						</td>
						<td style="text-align: center">
								<?= $alternatif['kode'] ?>
						</td>
						<td>
								<?= $alternatif['namalengkap'] ?>
						</td>
						<td style="text-align: center">
								<?= number_format($alternatif['hasil_wp'],5) ?>
						</td>
                </tr>
            <?php
                }
            ?>
        </tbody>
			</table>
			</div>
	</div>
		
	
	<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
	<script type="text/javascript">
		$(document).ready( function () {
				$('#table1').DataTable();
		} );
	</script>
	<script type="text/javascript">
		$(document).ready( function () {
				$('#table2').DataTable();
		} );
	</script>
	<script type="text/javascript">
		$(document).ready( function () {
				$('#table3').DataTable();
		} );
	</script>
	<script type="text/javascript">
		$(document).ready( function () {
				$('#table4').DataTable();
		} );
	</script>
	<script type="text/javascript">
		$(document).ready( function () {
				$('#table5').DataTable();
		} );
	</script>
	<script type="text/javascript">
		$(document).ready( function () {
				$('#table6').DataTable();
		} );
	</script>
	<!-- Footer -->
    <footer>
      <div class="container">
        <div class="row">
             <p class="m-0 text-center text-white">Copyright &copy; Maghfira Izzani Rahmayani || 16650017</p>
          </div>
        </div>
      </div>

 	</footer>
</body>

</html>
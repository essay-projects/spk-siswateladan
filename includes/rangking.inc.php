<?php
    class Bobot{
        private $conn;
        private $table_name = 'rangking';

        public $kode;
        public $id_kriteria;
        public $nilai_rangking;

        public function __construct($db)
        {
            $this->conn = $db;
        }

        //fungsi menampilkan semua data 
        function readAll(){
            $query = "SELECT * FROM ".$this->table_name." ORDER BY kode ASC";
            $stmt = $this->conn->prepare($query);
            $stmt->execute();
            return $stmt;
        }
        // fungsi memanggil nilai berdsarkan id alternatif
        function readR($a){
            $query = " SELECT * FROM alternatif a, kriteria k, rangking r WHERE a.kode=r.kode and k.id_kriteria=r.id_kriteria and r.kode='$a'";
            $stmt =$this->conn->prepare($query);
            $stmt->execute();
            
            return $stmt;
        }
         // fungsi untuk mencari nilai min saw
        function readMax($b){    
            $query = " SELECT max(nilai_rangking) as max FROM " .$this->table_name. " WHERE id_kriteria='$b' LIMIT 0,1";
            $stmt =$this->conn->prepare($query);
            $stmt->execute();
            
            return $stmt;
        }
        // fungsi untuk mencari nilai max saw
        function readMin($b){    
            $query = " SELECT min(nilai_rangking) as min FROM " .$this->table_name. " WHERE id_kriteria='$b' LIMIT 0,1";
            $stmt =$this->conn->prepare($query);
            $stmt->execute();
            
            return $stmt;
        }
        

        function readBobot(){
            $query = "SELECT nilai_rangking FROM ".$this->table_name." 
                    WHERE 
                        id_kriteria = ".$this->id_kriteria." 
                    AND 
                        kode = ".$this->kode;
            $stmt = $this->conn->prepare($query);
            $stmt->execute();
            
            $result = $stmt->fetch(PDO::FETCH_ASSOC);
    
            $bobot = $result['nilai_rangking'];
    
            return $bobot;
        }
        // READ BOBOT
        // function readBobot(){
        //     $query = "SELECT nilai_rangking FROM ".$this->table_name." 
        //     WHERE 
        //         id_kriteria = ".$this->id_kriteria." 
        //     AND
        //         kode = ".$this->kode.";";
        //     $stmt = $this->conn->query($query);
        //     $result->$stmt->fetch();

        //     $bobot = $result[0];
            
        //     return $bobot;
        // }

        function insertBobot($arrData) {
            $query = "insert into ".$this->table_name." (kode,id_kriteria,nilai_rangking) VALUES (?,?,?)";
            $stmt = $this->conn->prepare($query);
            $stmt->bindParam(1,$arrData[0]);
            $stmt->bindParam(2,$arrData[1]);
            $stmt->bindParam(3,$arrData[2]);
            $err=$stmt->execute();


            return $err;
        }

        function updateBobot($arrData) {
            $query = "update ".$this->table_name." set nilai_rangking=? Where kode=? and id_kriteria=?";
            $stmt = $this->conn->prepare($query);
            $stmt->bindParam(1,$arrData[0]);
            $stmt->bindParam(2,$arrData[1]);
            $stmt->bindParam(3,$arrData[2]);
            $err=$stmt->execute();


            return $err;
        }

        function insertAnalisaSensistifitas($kode) {
            $query = "INSERT INTO `hasil`(`kode`, `hasil_wp`, `hasil_wp_sensitifitas`, `hasil_saw`, `hasil_saw_sensitifitas`, `rangking_wp`, `rangking_wp_sensitifitas`, `rangking_saw`, `rangking_saw_sensitifitas`) VALUES (?,0,0,0,0,0,0,0,0,0)";
            $stmt = $this->conn->prepare($query);
            $stmt->bindParam(1,$kode);
            $err=$stmt->execute();


            return $err;
            
        }
    }
?>




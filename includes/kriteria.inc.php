<?php
    class Kriteria{
        private $conn;
        private $table_name = 'kriteria';

        public $id_kriteria;
        public $nama_kriteria;
        public $tipe_kriteria;
        public $bobot_kriteria;

        public function __construct($db)
        {
            $this->conn = $db;
        }

        function maxAlternativ(){
            $query = "SELECT MAX(id_kriteria) FROM ".$this->table_name.";";
            $stmt = $this->conn->query($query);
            $result = $stmt->fetch();

            $count = $result[0];

            return $count;
        }

        //fungsi read data
        function readAll(){
            $query = "SELECT * FROM ".$this->table_name." ORDER BY id_kriteria ASC";
            $stmt = $this->conn->prepare($query);
            $stmt->execute();

            return $stmt;
        }
        
        // fungsi tipe berdasarkan kriteria
        function readTipe(){
            $query = "SELECT tipe_kriteria FROM ".$this->table_name." WHERE id_kriteria = ".$this->id_kriteria.";";
            $stmt = $this->conn->query($query);
            $result = $stmt->fetch();
            $tipe = $result[0];
            return $tipe;
        }

        function readSumBobot(){    
            $query = " SELECT SUM(bobot_kriteria) as sum FROM " .$this->table_name;
            $stmt =$this->conn->prepare($query);
            $stmt->execute();
            
            return $stmt;
        }
        function readSum(){    
            $query = " SELECT SUM(bobot_sensitifitas) as sum FROM " .$this->table_name;
            $stmt =$this->conn->prepare($query);
            $stmt->execute();
            
            return $stmt;
        }
        function insertKriteria($arrData) {
            $query = "insert into ".$this->table_name." (nama_kriteria,tipe_kriteria,bobot_kriteria,bobot_sensitifitas) VALUES (?,?,?,?)";
            $stmt = $this->conn->prepare($query);
            $stmt->bindParam(1,$arrData[0]);
            $stmt->bindParam(2,$arrData[1]);
            $stmt->bindParam(3,$arrData[2]);
            $stmt->bindParam(4,$arrData[3]);  
            $err=$stmt->execute();


            return ['id_kriteria'=>$this->maxAlternativ(), 'is_success'=>$err];
    }
     function readOne(){
            $query = "SELECT * FROM " .$this->table_name. " WHERE id_kriteria=?";
            $stmt = $this->conn->prepare($query);
            $stmt->bindParam(1, $this->id_kriteria);
            $stmt->execute();

            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            
            $this->nama_kriteria = $row['nama_kriteria'];
            $this->tipe_kriteria = $row['tipe_kriteria'];
            $this->bobot_kriteria = $row['bobot_kriteria'];
            $this->bobot_sensitifitas = $row['bobot_sensitifitas'];
        }
        function updateKriteria($arrData) {
            $query = "UPDATE ".$this->table_name." set nama_kriteria=?,tipe_kriteria=?,bobot_kriteria=?,bobot_sensitifitas=? WHERE id_kriteria=?";
            $stmt = $this->conn->prepare($query);
            $stmt->bindParam(1,$arrData[0]);
            $stmt->bindParam(2,$arrData[1]);
            $stmt->bindParam(3,$arrData[2]);
            $stmt->bindParam(4,$arrData[3]);
            $stmt->bindParam(5,$arrData[4]);
            $err=$stmt->execute();


            return $err;
        }
         function deleteKriteria($id_kriteria) {
            $query = "
            DELETE FROM kriteria WHERE id_kriteria=? 
            ";
            $stmt = $this->conn->prepare($query);
            $stmt->bindParam(1,$id_kriteria);
            $err=$stmt->execute();
            return $err;
        }
        
}
?>
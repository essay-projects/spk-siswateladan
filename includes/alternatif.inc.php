<?php
    class Alternatif{
        private $conn;
        private $table_name = 'alternatif';

        public $kode;
        public $namalengkap;
        public $alamat;
        public $angkatan;
        public $kelas;
        public function __construct($db)
        {
            $this->conn = $db;
        }



        //membaca semua data alternatif
        function readAll(){
            
            $query = "SELECT * FROM ".$this->table_name." ORDER BY kode ASC";
            $stmt = $this->conn->prepare($query);
            $stmt->execute();

            return $stmt;
        }
        function readOne(){
            $query = "SELECT * FROM " .$this->table_name. " WHERE kode=?";
            $stmt = $this->conn->prepare($query);
            $stmt->bindParam(1, $this->kode);
            $stmt->execute();

            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            
            $this->namalengkap = $row['namalengkap'];
            $this->alamat = $row['alamat'];
            $this->angkatan = $row['angkatan'];
            $this->kelas = $row['kelas'];
        }
        
        function readId(){
            $query = "SELECT kode FROM ".$this->table_name.";";
            $stmt = $this->conn->prepare($query);
            $stmt->execute();

            $kode = $stmt->fetchAll(PDO::FETCH_COLUMN, 0);
            return $kode;
        }

        function jmlAlternativ(){
            $query = "SELECT COUNT(kode) FROM ".$this->table_name.";";
            $stmt = $this->conn->query($query);
            $result = $stmt->fetch();

            $count = $result[0];

            return $count;
        }

        function maxAlternativ(){
            $query = "SELECT MAX(kode) FROM ".$this->table_name.";";
            $stmt = $this->conn->query($query);
            $result = $stmt->fetch();

            $count = $result[0];

            return $count;
        }

        function insertAlternatif($arrData) {
            $query = "insert into ".$this->table_name." (namalengkap,alamat,angkatan,kelas) VALUES (?,?,?,?)";
            $stmt = $this->conn->prepare($query);
            $stmt->bindParam(1,$arrData[0]);
            $stmt->bindParam(2,$arrData[1]);
            $stmt->bindParam(3,$arrData[2]);
            $stmt->bindParam(4,$arrData[3]);    
            $err=$stmt->execute();


            return ['kode'=>$this->maxAlternativ(), 'is_success'=>$err];
        }

        function updateAlternatif($arrData) {
            $query = "update ".$this->table_name." set namalengkap=?,alamat=?,angkatan=?,kelas=? Where kode=?";
            $stmt = $this->conn->prepare($query);
            $stmt->bindParam(1,$arrData[0]);
            $stmt->bindParam(2,$arrData[1]);
            $stmt->bindParam(3,$arrData[2]);
            $stmt->bindParam(4,$arrData[3]);
            $stmt->bindParam(5,$arrData[4]);
            $stmt->bindParam(6,$arrData[5]);
            $err=$stmt->execute();


            return $err;
        }

        function deleteAlternatif($kode) {
            $query = "
            DELETE alternatif, hasil, rangking 
                FROM alternatif
                LEFT JOIN hasil ON alternatif.kode = hasil.kode
                LEFT JOIN rangking ON alternatif.kode = rangking.kode
            WHERE alternatif.kode=? 
            ";
            $stmt = $this->conn->prepare($query);
            $stmt->bindParam(1,$kode);
            $err=$stmt->execute();
            return $err;
        }
        
    }
?>